package com.zell_mbc.medilog.data

import androidx.lifecycle.LiveData

class DataRepository(private val dataType: Int, private val dao: DataDao, fS:Long, fE: Long){ //: DataRepository() {
    var filterStart = 0L
    var filterEnd = 0L

    init {
        filterStart = fS
        filterEnd = fE
    }

    fun getItems(filterStart: Long, filterEnd: Long): LiveData<List<Data>> {
        return if ((filterStart != 0L) && (filterEnd == 0L)) dao.getItemsGreaterThan(dataType, filterStart)
        else if ((filterStart == 0L) && (filterEnd != 0L)) dao.getItemsLessThan(dataType, filterEnd)
        else if ((filterStart != 0L) && (filterEnd != 0L)) dao.getItemsRange(dataType, filterStart, filterEnd)
        else dao.getItems(dataType)
    }

    fun getItems(order: String, filterStart: Long, filterEnd: Long): List<Data> {
        if (order == "ASC") {
            if ((filterStart != 0L) && (filterEnd == 0L)) return dao.getItemsASCGreaterThan(dataType, filterStart)
            if ((filterStart == 0L) && (filterEnd != 0L)) return dao.getItemsASCLessThan(dataType, filterEnd)
            if ((filterStart != 0L) && (filterEnd != 0L)) return dao.getItemsASCRange(dataType, filterStart, filterEnd)
            return dao.getItemsASC(dataType)
        } else {
            if ((filterStart != 0L) && (filterEnd == 0L)) return dao.getItemsDESCGreaterThan(dataType, filterStart)
            if ((filterStart == 0L) && (filterEnd != 0L)) return dao.getItemsDESCLessThan(dataType, filterEnd)
            if ((filterStart != 0L) && (filterEnd != 0L)) return dao.getItemsDESCRange(dataType, filterStart, filterEnd)
            return dao.getItemsDESC(dataType)
        }
    }

    fun getFirst(filterStart: Long, filterEnd: Long): Data? {
        val items = getItems("ASC", filterStart, filterEnd )
        if (items.isEmpty()) return null
        return items[0]
    }

    fun getLast(filterStart: Long, filterEnd: Long): Data? {
        val items = getItems("DESC", filterStart, filterEnd )
        if (items.isEmpty()) return null
        return items[0]
    }

    fun getSize(filterStart: Long, filterEnd: Long): Int {
        if ((filterStart != 0L) && (filterEnd == 0L)) return dao.getSizeGreaterThan(dataType, filterStart)
        if ((filterStart == 0L) && (filterEnd != 0L)) return dao.getSizeLessThan(dataType, filterEnd)
        if ((filterStart != 0L) && (filterEnd != 0L)) return dao.getSizeRange(dataType, filterStart, filterEnd)
        return dao.getSize(dataType)
    }

    fun getDBSize(): Int = dao.getDBSize()
    fun getSize(type: Int): Int = dao.getSize(type)

    fun getItem(index: Int): Data = dao.getItem(index)

    fun getMinValue1(filterStart: Long, filterEnd: Long): Float {
        if ((filterStart != 0L) && (filterEnd == 0L)) return dao.getMinValue1GreaterThan(dataType, filterStart)
        if ((filterStart == 0L) && (filterEnd != 0L)) return dao.getMinValue1LessThan(dataType, filterEnd)
        if ((filterStart != 0L) && (filterEnd != 0L)) return dao.getMinValue1Range(dataType, filterStart, filterEnd)
        return dao.getMinValue1(dataType)
    }

    fun getMaxValue1(filterStart: Long, filterEnd: Long): Float {
        if ((filterStart != 0L) && (filterEnd == 0L)) return dao.getMaxValue1GreaterThan(dataType, filterStart)
        if ((filterStart == 0L) && (filterEnd != 0L)) return dao.getMaxValue1LessThan(dataType, filterEnd)
        if ((filterStart != 0L) && (filterEnd != 0L)) return dao.getMaxValue1Range(dataType, filterStart, filterEnd)
        return dao.getMaxValue1(dataType)
    }

    fun getMinValue2(filterStart: Long, filterEnd: Long): Float {
        if ((filterStart != 0L) && (filterEnd == 0L)) return dao.getMinValue2GreaterThan(dataType, filterStart)
        if ((filterStart == 0L) && (filterEnd != 0L)) return dao.getMinValue2LessThan(dataType, filterEnd)
        if ((filterStart != 0L) && (filterEnd != 0L)) return dao.getMinValue2Range(dataType, filterStart, filterEnd)
        return dao.getMinValue2(dataType)
    }

    fun getMaxValue2(filterStart: Long, filterEnd: Long): Float {
        if ((filterStart != 0L) && (filterEnd == 0L)) return dao.getMaxValue2GreaterThan(dataType, filterStart)
        if ((filterStart == 0L) && (filterEnd != 0L)) return dao.getMaxValue2LessThan(dataType, filterEnd)
        if ((filterStart != 0L) && (filterEnd != 0L)) return dao.getMaxValue2Range(dataType, filterStart, filterEnd)
        return dao.getMaxValue2(dataType)
    }

    suspend fun insert(item: Data): Long { return dao.insert(item) }
    suspend fun update(item: Data) { dao.update(item) }

    suspend fun delete(id: Int) = dao.delete(id)
    suspend fun deleteAll() = dao.deleteAll()
    suspend fun deleteAll(dataType: Int) = dao.deleteAll(dataType)
    suspend fun deleteAllFiltered(filterStart: Long, filterEnd: Long) = dao.deleteAllFiltered(filterStart, filterEnd)
    suspend fun deleteTmpItem(tmpComment: String) = dao.deleteTmpItem(tmpComment)

    // Return values summed up by day
    fun getDay(day: String):LiveData<Int> = dao.getDay(dataType, day)

    fun getDays(filterStart: Long, filterEnd: Long):List<Data> {
        return if ((filterStart != 0L) && (filterEnd == 0L)) dao.getDaysGreaterThan(dataType, filterStart)
        else if ((filterStart == 0L) && (filterEnd != 0L)) dao.getDaysLessThan(dataType, filterEnd)
        else if ((filterStart != 0L) && (filterEnd != 0L)) dao.getDaysRange(dataType, filterStart, filterEnd)
        else dao.getDays(dataType)
    }

    fun dataBackup() = dao.backup()
}