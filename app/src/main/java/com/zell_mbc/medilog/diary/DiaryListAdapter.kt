package com.zell_mbc.medilog.diary

import android.content.Context
import android.graphics.Color
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utillity.Preferences
import java.text.DateFormat


class DiaryListAdapter internal constructor(context: Context) : RecyclerView.Adapter<DiaryListAdapter.DiaryViewHolder>() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var clickListener: ItemClickListener? = null

    private var items = emptyList<Data>() // Cached copy of diarys, no ide why I need it

    private val textSize: Float
    private var dateFormat: DateFormat
    private var timeFormat: DateFormat
    private var marker = ""

    private val highlightValues: Boolean

    inner class DiaryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val tvState: TextView = itemView.findViewById(R.id.tvState)
        val tvDate: TextView = itemView.findViewById(R.id.tvDate)
        val tvDiary: TextView = itemView.findViewById(R.id.tvDiary)

        override fun onClick(view: View) {
            if (clickListener != null) clickListener!!.onItemClick(view, adapterPosition)
        }

        init {
            tvDate.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
            tvDiary.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
            tvState.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
            itemView.setOnClickListener(this)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DiaryViewHolder {
        val itemView = inflater.inflate(R.layout.diaryview_row, parent, false)
        return DiaryViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: DiaryViewHolder, position: Int) {
        val current = items[position]
        holder.tvDiary.text = current.value1

        val highlight = try {
            current.value2.toInt()
        }
        catch  (e: NumberFormatException) {
            0
        }

        if (highlight > 0) holder.tvState.text = marker
        else holder.tvState.text = ""

        when (highlight) {
            1 -> holder.tvState.setTextColor(Color.rgb(255, 191, 0))
            2 -> holder.tvState.setTextColor(Color.RED) //Color.rgb(255, 0, 0))
        }
        val tmpString = dateFormat.format(current.timestamp) + " - " + timeFormat.format(current.timestamp)
        holder.tvDate.text = tmpString
    }

    internal fun setItems(items: List<Data>) {
        this.items = items
        notifyDataSetChanged()
    }

    fun getItemAt(position: Int): Data {
        return items[position]
    }

    // allows clicks events to be caught
    fun setClickListener(itemClickListener: ItemClickListener?) {
        clickListener = itemClickListener
    }


    // parent activity will implement this method to respond to click events
    interface ItemClickListener {
        fun onItemClick(view: View?, position: Int)
    }

    override fun getItemCount() = items.size

    init {
        val preferences = Preferences.getSharedPreferences(context)
        dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
        timeFormat = DateFormat.getTimeInstance(DateFormat.SHORT)
        textSize = java.lang.Float.valueOf(preferences.getString(SettingsActivity.KEY_PREF_TEXT_SIZE, "15")!!)
        highlightValues = preferences.getBoolean(SettingsActivity.KEY_PREF_COLOUR, false)
        marker = context.getString(R.string.diaryMarker)
    }
}
