package com.zell_mbc.medilog.diary

import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import com.zell_mbc.medilog.*
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.data.TabFragment
import com.zell_mbc.medilog.databinding.DiaryTabBinding
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utillity.Preferences
import com.zell_mbc.medilog.weight.WeightEditActivity
import java.text.DateFormat
import java.util.*

class DiaryFragment : TabFragment(), DiaryListAdapter.ItemClickListener {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: DiaryTabBinding? = null
    private val binding get() = _binding!!

    private var adapter: DiaryListAdapter? = null

    private val GOOD = 0
    private val NOT_GOOD = 1
    private val BAD = 2
    private var state = GOOD
    private val activityClass = DiaryEditActivity::class.java

    private var standardColors = 0 // : Int //ColorStateList

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = DiaryTabBinding.inflate(inflater,container,false)
        initializeService(binding.root)

        return binding.root
    }

    override fun editItem(index: Int) {
        val intent = Intent(requireContext(), activityClass)
        launchActivity(intent, index)
//        launchActivity(activityClass, index)
    }

    private fun addItem() {
        if (!quickEntry) {
            val tmpItem = Data(0, Date().time, MainActivity.tmpComment, viewModel.dataType, "","","", "")
            val itemID = viewModel.insert(tmpItem)
            if (itemID  != 0L) editItem(itemID.toInt())
            else userOutputService.showMessageAndWaitForLong("No entry with tmp value found!")
            return
        }

        // Check empty variables
        val value = binding.etDiary.text.toString()
        if (value.isEmpty()) {
            val v = view
            if (v != null) {
                userOutputService.showMessageAndWaitForLong(getString(R.string.diaryMissing))
            }
            return
        }

        val item = Data(0, Date().time, "", viewModel.dataType, value,state.toString(),"","")
        viewModel.insert(item)
        if ((viewModel.filterEnd > 0 ) && (viewModel.filterEnd < item.timestamp)) userOutputService.showMessageAndWaitForDuration(getString(R.string.filteredOut), 4000)

        state = GOOD
        binding.etDiary.setText("")

        // Close keyboard after entry is done
        (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken, 0)
    }

    private fun setColour() {
        if (state == BAD) state = GOOD
        else              state++

        when (state) {
            NOT_GOOD -> binding.btSetState.setColorFilter(Color.rgb(255, 191, 0)) //backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryAmber))
            BAD      -> binding.btSetState.setColorFilter(Color.RED) //backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryRed))
            else     -> binding.btSetState.setColorFilter(standardColors)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setColourStyle(binding.btAdd)
        binding.btAdd.setColorFilter(Color.WHITE)

        standardColors =  binding.etDiary.currentHintTextColor //  textColors.defaultColor
        binding.btSetState.setColorFilter(standardColors) // Make sure the button comes up in correct colour
//        Log.d("DiaryFragment onCreateView : ", "standardColors " + standardColors )

        // Hide quick entry fields if needed
        if (!quickEntry) {
            binding.etDiary.visibility = View.GONE
            binding.btSetState.visibility = View.GONE
        }
        else binding.etDiary.visibility = View.VISIBLE

        // set up the RecyclerView
        val layoutManager = LinearLayoutManager(requireContext())
        binding.rvDiaryList.layoutManager = layoutManager

        adapter = DiaryListAdapter(requireContext())
        viewModel = ViewModelProvider(requireActivity()).get(DiaryViewModel::class.java)
        viewModel.items.observe(requireActivity(), { diary -> diary?.let { adapter!!.setItems(it) } })

        adapter!!.setClickListener(this)
        binding.rvDiaryList.adapter = adapter

        // Respond to click events
        binding.btAdd.setOnClickListener { addItem() }
        binding.btSetState.setOnClickListener { setColour() }

        setColourStyle(binding.btInfo, true)

        binding.btInfo.setOnClickListener(View.OnClickListener {
            context ?: return@OnClickListener
            val intent = Intent(requireContext(), DiaryInfoActivity::class.java)
            startActivity(intent)
        })

        binding.etDiary.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if ((event.action == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                addItem()
                return@OnKeyListener true
            }
            false
        })

    }

    override fun onItemClick(view: View?, position: Int) {
        val item = adapter?.getItemAt(position)
        item ?: return
        itemClicked(item)
    }

}