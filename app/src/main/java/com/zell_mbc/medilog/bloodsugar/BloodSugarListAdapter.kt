package com.zell_mbc.medilog.bloodsugar

import android.content.Context
import android.graphics.Color
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zell_mbc.medilog.MainActivity.Companion.BLOODSUGAR_LOWER_THRESHOLD_DEFAULT
import com.zell_mbc.medilog.MainActivity.Companion.BLOODSUGAR_UNIT_DEFAULT
import com.zell_mbc.medilog.MainActivity.Companion.BLOODSUGAR_UPPER_THRESHOLD_DEFAULT
import com.zell_mbc.medilog.MainActivity.Companion.TEXT_SIZE_DEFAULT
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utillity.Preferences
import java.text.DateFormat

class BloodSugarListAdapter internal constructor(context: Context) : RecyclerView.Adapter<BloodSugarListAdapter.BloodSugarViewHolder>() {
        private val inflater: LayoutInflater = LayoutInflater.from(context)
        private var clickListener: ItemClickListener? = null

        private var items = emptyList<Data>() // Cached copy of bloodSugars, no ide why I need it

        private var textSize = TEXT_SIZE_DEFAULT.toFloat()
        private var dateFormat: DateFormat
        private var timeFormat: DateFormat
        private val highlightValues: Boolean
        private val bloodSugarUnit: String
        private var bloodSugarLowerThreshold = BLOODSUGAR_LOWER_THRESHOLD_DEFAULT.toInt()
        private var bloodSugarUpperThreshold = BLOODSUGAR_UPPER_THRESHOLD_DEFAULT.toInt()

    inner class BloodSugarViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val dateItem: TextView = itemView.findViewById(R.id.tvDateItem)
        val bloodSugarItem: TextView = itemView.findViewById(R.id.tvBloodSugarItem)
        val commentItem: TextView = itemView.findViewById(R.id.tvCommentItem)

        override fun onClick(view: View) {
            if (clickListener != null) clickListener!!.onItemClick(view, adapterPosition)
        }

        init {
            dateItem.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
            bloodSugarItem.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
            commentItem.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
            itemView.setOnClickListener(this)
        }
    }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BloodSugarViewHolder {
            val itemView = inflater.inflate(R.layout.bloodsugarview_row, parent, false)
            return BloodSugarViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: BloodSugarViewHolder, position: Int) {
            val orgColor = holder.dateItem.textColors.defaultColor

            val current = items[position]
            val tmpString = dateFormat.format(current.timestamp) + " - " + timeFormat.format(current.timestamp)
            holder.dateItem.text = tmpString

            val glycol = try {
                current.value1.toInt()
            } catch  (e: NumberFormatException) {
                0
            }

            val s = current.value1 + bloodSugarUnit
            if (highlightValues) {
                if (glycol < bloodSugarLowerThreshold || glycol > bloodSugarUpperThreshold) {
 //                   Log.d("BloodSugarListAdapter: ", "Color bloodSugarThreshold " + bloodSugarThreshold + " f: " + f)
                    holder.bloodSugarItem.setTextColor(Color.rgb(255, 0, 0))
                    holder.bloodSugarItem.text = s
                } else {
                    holder.bloodSugarItem.setTextColor(orgColor)
                    holder.bloodSugarItem.text = s
//                    Log.d("BloodSugarListAdapter: ", "noColor bloodSugarThreshold " + bloodSugarThreshold + " f: " + f)
                }
            }
            else holder.bloodSugarItem.text = s

            holder.commentItem.text = current.comment
        }

        internal fun setItems(items: List<Data>) {
            this.items = items
            notifyDataSetChanged()
        }

        fun getItemAt(position: Int): Data {
            return items[position]
        }

    // allows clicks events to be caught
    fun setClickListener(itemClickListener: ItemClickListener?) {
        clickListener = itemClickListener
    }


    // parent activity will implement this method to respond to click events
    interface ItemClickListener {
        fun onItemClick(view: View?, position: Int)
    }

    override fun getItemCount() = items.size

    init {
        val preferences = Preferences.getSharedPreferences(context)
        bloodSugarUnit = " " + preferences.getString(SettingsActivity.KEY_PREF_BLOODSUGAR_UNIT, BLOODSUGAR_UNIT_DEFAULT)
        dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
        timeFormat = DateFormat.getTimeInstance(DateFormat.SHORT)

        highlightValues = preferences.getBoolean(SettingsActivity.KEY_PREF_COLOUR, false)

        var s = preferences.getString(SettingsActivity.KEY_PREF_TEXT_SIZE, TEXT_SIZE_DEFAULT)
        if (s != null) {
            textSize = try { s.toFloat() }
            catch  (e: NumberFormatException) { TEXT_SIZE_DEFAULT.toFloat() }
        }

        s = preferences.getString(SettingsActivity.KEY_PREF_BLOODSUGAR_LOWER_THRESHOLD, BLOODSUGAR_LOWER_THRESHOLD_DEFAULT)
        if (s != null) {
            bloodSugarLowerThreshold = try { s.toInt() }
            catch  (e: NumberFormatException) { BLOODSUGAR_LOWER_THRESHOLD_DEFAULT.toInt() }
        }
        s = preferences.getString(SettingsActivity.KEY_PREF_BLOODSUGAR_UPPER_THRESHOLD, BLOODSUGAR_UPPER_THRESHOLD_DEFAULT)
        if (s != null) {
            bloodSugarUpperThreshold = try { s.toInt() }
            catch  (e: NumberFormatException) { BLOODSUGAR_UPPER_THRESHOLD_DEFAULT.toInt() }
        }
    }
}
