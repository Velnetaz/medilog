package com.zell_mbc.medilog.bloodsugar

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.DashPathEffect
import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.androidplot.util.PixelUtils
import com.androidplot.xy.*
import com.zell_mbc.medilog.MainActivity.Companion.BLOODSUGAR
import com.zell_mbc.medilog.MainActivity.Companion.BLOODSUGAR_LOWER_THRESHOLD_DEFAULT
import com.zell_mbc.medilog.MainActivity.Companion.BLOODSUGAR_UPPER_THRESHOLD_DEFAULT
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utillity.*
import java.text.*
import java.util.*
import kotlin.math.roundToInt

// Chart manual
// https://github.com/halfhp/androidplot/blob/master/docs/xyplot.md
class BloodSugarChartFragment : Fragment() {
    private var bloodSugarLowerThreshold = ArrayList<Float>()
    private var bloodSugarUpperThreshold = ArrayList<Float>()
    private var bloodSugars = ArrayList<Float>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.bloodsugar_chart, container, false)
    }

    @SuppressLint("SimpleDateFormat")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // create a couple arrays of y-values to plot:
        val labels = ArrayList<String>()
        var wMax = 0f
        var wMin = 1000f

        val preferences = Preferences.getSharedPreferences(requireContext())
        var sTmp: String

        var fTmp: Float
        val simpleDate = SimpleDateFormat("MM-dd")

        val viewModel = ViewModelProvider(this).get(BloodSugarViewModel::class.java)
        viewModel.init(BLOODSUGAR)
        val items = viewModel.getItems("ASC", filtered = true)
        for (wi in items) {
            sTmp = simpleDate.format(wi.timestamp)

            labels.add(sTmp)
            bloodSugars.add(wi.value1.toFloat())

            // Keep min and max values
            fTmp = wi.value1.toFloat()
            if (fTmp > wMax) {
                wMax = fTmp
            }
            if (fTmp < wMin) {
                wMin = fTmp
            }
        }
        if (bloodSugars.size == 0) {
            return
        }

        // If threshold is set create dedicated chart, otherwise show as origin
        val thresholds = preferences.getBoolean(SettingsActivity.KEY_PREF_showBloodSugarThresholds, false)
        if (thresholds) {
            var bloodSugarLowerThresholdValue = 0f
            var bloodSugarUpperThresholdValue = 0f
            var s = preferences.getString(SettingsActivity.KEY_PREF_BLOODSUGAR_LOWER_THRESHOLD, BLOODSUGAR_LOWER_THRESHOLD_DEFAULT)
            if (!s.isNullOrEmpty()) bloodSugarLowerThresholdValue = s.toFloat()

            s = preferences.getString(SettingsActivity.KEY_PREF_BLOODSUGAR_UPPER_THRESHOLD,BLOODSUGAR_UPPER_THRESHOLD_DEFAULT)
            if (!s.isNullOrEmpty()) bloodSugarUpperThresholdValue = s.toFloat()

            for (item in bloodSugars) {
                bloodSugarLowerThreshold.add(bloodSugarLowerThresholdValue)
                bloodSugarUpperThreshold.add(bloodSugarUpperThresholdValue)
            }
        }

        // initialize our XYPlot reference:
        val plot: XYPlot = view.findViewById(R.id.bloodSugarPlot)
        PanZoom.attach(plot)

        val backgroundColor = getBackgroundColor(requireContext())
        plot.graph.backgroundPaint.color = backgroundColor

        val fontSizeSmall: Int = getFontSizeSmallInPx(requireContext())

        val axisPaint = Paint()
        axisPaint.style = Paint.Style.FILL_AND_STROKE
        axisPaint.color = getTextColorPrimary(requireContext())
        axisPaint.textSize = fontSizeSmall.toFloat()
        axisPaint.isAntiAlias = true

        val gridPaint = Paint()
        gridPaint.style = Paint.Style.STROKE
        gridPaint.color = getTextColorSecondary(requireContext())
        gridPaint.isAntiAlias = false
        gridPaint.pathEffect = DashPathEffect(floatArrayOf(3f, 2f), 0F)

        val showGrid = preferences.getBoolean(SettingsActivity.KEY_PREF_showBloodSugarGrid, true)
        if (!showGrid) {
            plot.graph.domainGridLinePaint = null
            plot.graph.rangeGridLinePaint = null
        }
        else {
            plot.graph.domainGridLinePaint = gridPaint
            plot.graph.rangeGridLinePaint = gridPaint // Horizontal lines
        }

        val isLegendVisible = preferences.getBoolean(SettingsActivity.KEY_PREF_showBloodSugarLegend, false)
        plot.legend.isVisible = isLegendVisible
        plot.legend.isDrawIconBackgroundEnabled = false

        val series1: XYSeries = SimpleXYSeries(bloodSugars, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, getString(R.string.bloodSugar))
        val series2: XYSeries = SimpleXYSeries(bloodSugarLowerThreshold, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "")
        val series3: XYSeries = SimpleXYSeries(bloodSugarUpperThreshold, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "")

        val wMinBoundary = wMin.roundToInt()
        val wMaxBoundary = wMax.roundToInt()
        plot.setRangeBoundaries(wMinBoundary, wMaxBoundary, BoundaryMode.FIXED)
        plot.setUserRangeOrigin(wMinBoundary/10*10) // Set to a 10er value
        plot.outerLimits.set( 0, bloodSugars.size-1, wMinBoundary, wMaxBoundary) // For pan&zoom

        plot.setRangeStep(StepMode.INCREMENT_BY_VAL, 5.0)
        if (series1.size() < 10) plot.setDomainStep(StepMode.SUBDIVIDE, series1.size().toDouble()) // Avoid showing the same day multiple times
        plot.graph.getLineLabelStyle(XYGraphWidget.Edge.LEFT).format = DecimalFormat("####") // + bloodSugarUnit));  // Set integer y-Axis label

        val bloodsugarBorder = ContextCompat.getColor(requireContext(), R.color.chart_sys_border)
        val bloodsugarFill = ContextCompat.getColor(requireContext(), R.color.chart_sys_fill)
        val series1Format = LineAndPointFormatter(bloodsugarBorder, null, bloodsugarFill, null)
        plot.addSeries(series1, series1Format)

        if (thresholds) {
            val formatThreshold = LineAndPointFormatter(Color.BLUE, null, null, null)
            formatThreshold.linePaint.pathEffect = DashPathEffect(floatArrayOf( // always use DP when specifying pixel sizes, to keep things consistent across devices:
                    PixelUtils.dpToPix(20f),
                    PixelUtils.dpToPix(15f)), 0f)

            formatThreshold.isLegendIconEnabled = false
            formatThreshold.linePaint.strokeWidth = 2f
            plot.addSeries(series2, formatThreshold)
            plot.addSeries(series3, formatThreshold)
        }

        plot.graph.getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).format = object : Format() {
            override fun format(obj: Any, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer {
                val i = (obj as Number).toFloat().roundToInt()
                return toAppendTo.append(labels[i])
            }

            override fun parseObject(source: String, pos: ParsePosition): Any {
                return 0
            }
        }
    }
}
