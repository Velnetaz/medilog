package com.zell_mbc.medilog.bloodsugar

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class BloodSugarChartActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportFragmentManager.beginTransaction()
                .replace(android.R.id.content, BloodSugarChartFragment())
                .commit()
    }
}
