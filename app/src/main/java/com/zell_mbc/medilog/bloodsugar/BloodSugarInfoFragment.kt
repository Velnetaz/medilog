package com.zell_mbc.medilog.bloodsugar

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.utillity.Preferences
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.BLOODSUGAR
import com.zell_mbc.medilog.MainActivity.Companion.BLOODSUGAR_UNIT_DEFAULT
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.databinding.BloodsugarinfoformBinding
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity
import java.text.DateFormat

class BloodSugarInfoFragment : Fragment() {
    private var _binding: BloodsugarinfoformBinding? = null
    private val binding get() = _binding!!
    private lateinit var userOutputService : UserOutputService

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = BloodsugarinfoformBinding.inflate(inflater, container, false)
        initializeService(binding.root)

        return binding.root
    }
    private fun initializeService(view: View) {
        userOutputService = UserOutputServiceImpl(requireContext(),view)
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private lateinit var viewModel: BloodSugarViewModel  //by viewModels() //factoryProducer = { SavedStateViewModelFactory(this, ) })

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(requireActivity()).get(BloodSugarViewModel::class.java)
        viewModel.init(BLOODSUGAR)

        val count = viewModel.getSize(true)
        var item = viewModel.getLast(true)
        if (item == null) {
            userOutputService.showMessageAndWaitForLong(getString(R.string.noDataToShow))
            return
        }

        binding.tvBloodSugarHeader.text = ""// ""BloodSugar"         // Round to 2 digits

        val preferences = Preferences.getSharedPreferences(requireContext())
        val bloodSugarUnit = " " + preferences.getString(SettingsActivity.KEY_PREF_BLOODSUGAR_UNIT, BLOODSUGAR_UNIT_DEFAULT)

        var s = if ((viewModel.filterStart + viewModel.filterEnd) == 0L) getString(R.string.measurementsInDB) + " $count"
        else getString(R.string.measurementsInFilter) + " $count"
        binding.tvMeasurementCount.text = s

        val min = viewModel.getMinValue1(true).toInt()
        val max = viewModel.getMaxValue1(true).toInt()
        s = getString(R.string.minMaxValues) + " $min - $max $bloodSugarUnit"
        binding.tvMinMax.text = s

        val dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
        val endDate = dateFormat.format(item.timestamp)

        item = viewModel.getFirst(true)
        val startDate = dateFormat.format(item?.timestamp)

        s = getString(R.string.timePeriod) + " $startDate - $endDate"
        binding.tvTimePeriod.text = s
    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
        viewModel.deleteTmpItem() // Deletes all items with comment == tmpComment
    }
}
