package com.zell_mbc.medilog

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.ClipData
import android.content.Context
import android.content.Intent
import android.content.Intent.*
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.InputType
import android.text.InputType.TYPE_TEXT_VARIATION_PASSWORD
import android.util.Log
import android.view.*
import android.view.GestureDetector.SimpleOnGestureListener
import android.view.View.OnTouchListener
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.biometric.BiometricPrompt
import androidx.biometric.BiometricPrompt.PromptInfo
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.getColor
import androidx.core.net.toFile
import androidx.documentfile.provider.DocumentFile
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.input.input
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.zell_mbc.medilog.R.*
import com.zell_mbc.medilog.bloodpressure.BloodPressureFragment
import com.zell_mbc.medilog.bloodpressure.BloodPressureViewModel
import com.zell_mbc.medilog.bloodsugar.BloodSugarFragment
import com.zell_mbc.medilog.bloodsugar.BloodSugarViewModel
import com.zell_mbc.medilog.custom.CustomFragment
import com.zell_mbc.medilog.custom.CustomTabsEditActivity
import com.zell_mbc.medilog.custom.CustomViewModel
import com.zell_mbc.medilog.data.SettingsViewModel
import com.zell_mbc.medilog.data.ViewModel
import com.zell_mbc.medilog.databinding.ActivityMainBinding
import com.zell_mbc.medilog.diary.DiaryFragment
import com.zell_mbc.medilog.diary.DiaryViewModel
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.settings.SettingsActivity.Companion.KEY_PREF_AUTO_BACKUP
import com.zell_mbc.medilog.settings.SettingsActivity.Companion.KEY_PREF_BACKUP_WARNING
import com.zell_mbc.medilog.utillity.Preferences
import com.zell_mbc.medilog.water.WaterFragment
import com.zell_mbc.medilog.water.WaterViewModel
import com.zell_mbc.medilog.weight.WeightFragment
import com.zell_mbc.medilog.weight.WeightViewModel
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {
    private lateinit var preferences: SharedPreferences

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private lateinit var binding: ActivityMainBinding

    lateinit var mediLog: MediLog

    private lateinit var userOutputService: UserOutputService

    private val rcRESTORE = 9998
    private val rcBACKUP = 9999

    private var fragments = ArrayList<Fragment>()

    private var hideMenus = false
    private val mContext: Context = this

    private var dragSource = -1 // Hold the source tab in a Drag&Drop transaction

    @SuppressLint("SimpleDateFormat")
    private var csvPattern = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")


    override fun onCreate(savedInstanceState: Bundle?) {
        preferences = Preferences.getSharedPreferences(this)

        initializeServices()

        val window = this@MainActivity.window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)

        themeColour = preferences.getString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(string.blue))
        when (themeColour) {
            getString(string.green) -> {
                theme.applyStyle(style.AppThemeGreenNoBar, true)
                window.statusBarColor =  resources.getColor(color.colorPrimaryDarkGreen)
            }
            getString(string.red) -> {
                theme.applyStyle(style.AppThemeRedNoBar, true)
                window.statusBarColor =  resources.getColor(color.colorPrimaryDarkRed)
            }
            getString(string.gray) -> {
                theme.applyStyle(style.AppThemeGrayNoBar, true)
                window.statusBarColor =  resources.getColor(color.colorPrimaryDarkGray)
            }
            else -> {
                theme.applyStyle(style.AppThemeBlueNoBar, true)
                window.statusBarColor =  resources.getColor(color.colorPrimaryDarkBlue)
            }
        }

        super.onCreate(savedInstanceState) // For some reason the theme needs to be set before the super call

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

        mediLog = applicationContext as MediLog
        if (!preferences.getBoolean("DISCLAIMERCONFIRMED", false)) {
            // App runs for the first time -> set defaults
            val editor = preferences.edit()
            editor.putBoolean(SettingsActivity.KEY_PREF_SHOWTABTEXT, SHOW_TAB_TEXT_DEFAULT)
            editor.putBoolean(SettingsActivity.KEY_PREF_SHOWTABICON, SHOW_TAB_ICON_DEFAULT)
            editor.putBoolean(SettingsActivity.KEY_PREF_SCROLLABLETABS, SCROLL_TABS_DEFAULT)

            editor.putString(SettingsActivity.KEY_PREF_WEIGHT_UNIT,WEIGHT_UNIT_DEFAULT)
            editor.putString(SettingsActivity.KEY_PREF_WEIGHT_THRESHOLD,WEIGHT_THRESHOLD_DEFAULT)
            editor.putBoolean(SettingsActivity.KEY_PREF_SHOW_WEIGHT_THRESHOLD, SHOW_WEIGHT_THRESHOLD_DEFAULT)
            editor.putBoolean(SettingsActivity.KEY_PREF_SHOW_WEIGHT_GRID, SHOW_WEIGHT_GRID_DEFAULT)
            editor.putBoolean(SettingsActivity.KEY_PREF_SHOW_WEIGHT_LEGEND,SHOW_WEIGHT_LEGEND_DEFAULT)
            editor.putBoolean(SettingsActivity.KEY_PREF_WEIGHT_LINEAR_TRENDLINE, WEIGHT_LINEAR_TRENDLINE_DEFAULT)
            editor.putBoolean(SettingsActivity.KEY_PREF_WEIGHT_MOVING_AVERAGE_TRENDLINE, WEIGHT_MOVING_AVERAGE_TRENDLINE_DEFAULT)
            editor.putString(SettingsActivity.KEY_PREF_WEIGHT_MOVING_AVERAGE_SIZE, WEIGHT_MOVING_AVERAGE_SIZE_DEFAULT)
            editor.putBoolean(SettingsActivity.KEY_PREF_WEIGHT_DAY_STEPPING, KEY_PREF_WEIGHT_DAY_STEPPING_DEFAULT)
            editor.putBoolean(SettingsActivity.KEY_PREF_WEIGHT_BAR_CHART, KEY_PREF_WEIGHT_BAR_CHART_DEFAULT)

            editor.putBoolean(SettingsActivity.KEY_PREF_showBloodPressureThreshold, SHOW_BLOODPRESURE_THRESHOLD_DEFAULT)
            editor.putString(SettingsActivity.KEY_PREF_BLOODPRESSURE_UNIT, BLOODPRESSURE_UNIT_DEFAULT)

            editor.putBoolean(SettingsActivity.KEY_PREF_SHOW_WATER_THRESHOLD, SHOW_WATER_THRESHOLD_DEFAULT)
            editor.putString(SettingsActivity.KEY_PREF_WATER_THRESHOLD, WATER_THRESHOLD_DEFAULT)
            editor.putString(SettingsActivity.KEY_PREF_WATER_UNIT, WATER_UNIT_DEFAULT)
            editor.putBoolean(SettingsActivity.KEY_PREF_SUMMARYPDF, WATER_SHOW_PDF_SUMMARY_DEFAULT)

            editor.putString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(string.blue))
            editor.putString(KEY_PREF_BACKUP_WARNING, BACKUP_WARNING_DEFAULT)
            editor.putLong("LAST_BACKUP", Date().time) // Set current time to avoid a backup warning right after installation
            editor.apply()

            MaterialDialog(this).show {
                title(string.disclaimer)
                message(string.disclaimerText)
                positiveButton(string.agree) {
//                    val editor = preferences.edit()
                    editor.putBoolean("DISCLAIMERCONFIRMED", true)
                    editor.apply()
                }
            }
        }

        // Initialize Settings Table
        // We need this table for generic data types, unused for now
        val settings = ViewModelProvider(this).get(SettingsViewModel::class.java)
        settings.init()
        //########################################
        // Manually add new data type
  //      settings.deleteSettings()
 //       val lastType = settings.countDataTypes()
 //       val newType = BLOODSUGAR + lastType+1 // Last built-i type + the ones in the db + 1
//        settings.createPushUps("PushUps")
        //########################################

        setContentView(view)

        val activeTab = preferences.getInt("activeTab", 0)

        // Initialize delimiter on first run based on locale
        var delimiter = preferences.getString(SettingsActivity.KEY_PREF_DELIMITER, "")
        if (delimiter.equals("")) {
            val currentLanguage = Locale.getDefault().displayLanguage
            delimiter = if (currentLanguage.contains("Deutsch")) {
                // Central Europe
                ";"
            } else ","

            val editor = preferences.edit()
            editor.putString(SettingsActivity.KEY_PREF_DELIMITER, delimiter)
            editor.apply()
        }

        hideMenus = false

        loadActiveTabs()
        val adapter = TabAdapter(this)
        var tabHeader: String
        viewModels.clear()

        var dataType: Int

        for (type in activeTabs) {
            when (type) { // Known data types
                getString(string.weight) -> {
                    viewModels.add(ViewModelProvider(this).get(WeightViewModel::class.java))
                    fragments.add(WeightFragment())
                    tabHeader = getString(string.tab_weight)
                    dataType = WEIGHT
                }
                getString(string.bloodPressure) -> {
                    viewModels.add(ViewModelProvider(this).get(BloodPressureViewModel::class.java))
                    fragments.add(BloodPressureFragment())
                    tabHeader = getString(string.tab_bloodpressure)
                    dataType = BLOODPRESSURE
                }
                getString(string.diary) -> {
                    viewModels.add(ViewModelProvider(this).get(DiaryViewModel::class.java))
                    fragments.add(DiaryFragment())
                    tabHeader = getString(string.tab_diary)
                    dataType = DIARY
                }
                getString(string.water) -> {
                    viewModels.add(ViewModelProvider(this).get(WaterViewModel::class.java))
                    fragments.add(WaterFragment())
                    tabHeader = getString(string.tab_water)
                    dataType = WATER
                }
                getString(string.bloodSugar) -> {
                    viewModels.add(ViewModelProvider(this).get(BloodSugarViewModel::class.java))
                    fragments.add(BloodSugarFragment())
                    tabHeader = getString(string.tab_bloodsugar)
                    dataType = BLOODSUGAR
                }
                else -> {
                    // This will get fixed in v2.2
                    val msg = "Error in Tab $type"
                    userOutputService.showMessageAndWaitForLong(msg)
                    dataType = -1
                    // Todo: One ViewModel for all or unique ones?
                    //                 viewModels.add(ViewModelProviders.of(CustomFragment()).get(CustomViewModel::class.java))
/*                    viewModels.add(ViewModelProvider(this).get(CustomViewModel::class.java))
                    dataType = settings.getDataType(type)
                    fragments.add(CustomFragment()) */
                    tabHeader = "" //viewModels[viewModels.size - 1].get(dataType, "TAB_LABEL") */
                }
            }
            if (dataType > 0) {
                    viewModels[viewModels.size - 1].init(dataType)
                    adapter.addFragment(fragments[fragments.size - 1], tabHeader)
                    }
//                userOutputService.showMessageAndWaitForLong("Can't load tab for data type $type")
        }



        // No active tabs?
        if (viewModels.size == 0) {
            hideMenus = true
            userOutputService.showMessageAndWaitForLong(getString(string.noActiveTab))
        }

        binding.viewPager.adapter = adapter
        binding.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                invalidateOptionsMenu()
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {} // Define abstract function
            override fun onTabReselected(tab: TabLayout.Tab) {} // Define abstract function
        })

        val showTabText = preferences.getBoolean(SettingsActivity.KEY_PREF_SHOWTABTEXT, SHOW_TAB_TEXT_DEFAULT)
        val showTabIcon = preferences.getBoolean(SettingsActivity.KEY_PREF_SHOWTABICON, SHOW_TAB_ICON_DEFAULT)
        val scrollableTabs = preferences.getBoolean(SettingsActivity.KEY_PREF_SCROLLABLETABS, SCROLL_TABS_DEFAULT)

        // If both text and icon are displayed we need a bit more space
        if (showTabIcon && showTabText) binding.viewPager.setPadding(0, 56, 0, 0)

        adapter.showTitle(showTabText)
        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
            tab.text = adapter.getPageTitle(position)
        }.attach()

        // Set up drag and drop listeners
        for (pos in 0..binding.tabLayout.tabCount) {

            var longPress = false

            val gestureDetector = GestureDetector(object : SimpleOnGestureListener() {
                override fun onLongPress(e: MotionEvent) {
                    longPress = true
                }
            })

            class MyTouchListener : OnTouchListener {
                override fun onTouch(view: View, motionEvent: MotionEvent): Boolean {
                    gestureDetector.onTouchEvent(motionEvent)

                    return if (longPress) { //motionEvent.action == MotionEvent.ACTION_MOVE) {
                        val data = ClipData.newPlainText("", "")
                        val shadowBuilder = View.DragShadowBuilder(view)

                        @Suppress("DEPRECATION")
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) view.startDragAndDrop(data, shadowBuilder, view, 0)
                        else view.startDrag(data, shadowBuilder, view, 0)

                        val vv = view as (TabLayout.TabView)
                        if (vv.tab?.position != null) dragSource = vv.tab!!.position
                        true
                    } else {
                        false
                    }
                }
            }

            val v = binding.tabLayout.getTabAt(pos)?.view
            v?.setOnTouchListener(MyTouchListener())

            v?.setOnDragListener { vi, event ->
                when (event.action) {
                    DragEvent.ACTION_DROP -> {
                        val vv = vi as (TabLayout.TabView)
                        var dragTarget = -1
                        if (vv.tab?.position != null) dragTarget = vv.tab!!.position
                        reorderTabs(dragSource, dragTarget)
                    }
                }
                true
            }
        }

        if (scrollableTabs) binding.tabLayout.tabMode = TabLayout.MODE_SCROLLABLE
        else binding.tabLayout.tabMode = TabLayout.MODE_FIXED

        if (showTabIcon) {
            for (i in 0..viewModels.size) {
                binding.tabLayout.getTabAt(i)?.setIcon(viewModels[i].tabIcon)
            }
        }

        binding.viewPager.currentItem = activeTab

        when (preferences.getString(SettingsActivity.KEY_PREF_TABTRANSITIONS, "")) {
            "Card Flip" -> binding.viewPager.setPageTransformer(cardFlip)
            "Horizontal Flip" -> binding.viewPager.setPageTransformer(horizontalFlipTransformation)
            "Cube" -> binding.viewPager.setPageTransformer(Cube())
            "Depth" -> binding.viewPager.setPageTransformer(DepthTransformation())
//            "Hinge"            -> viewPager.setPageTransformer(HingeTransformation())
            else -> binding.viewPager.setPageTransformer(null) // Default = Horizontal slide
        }

        val toolbar = findViewById<Toolbar>(id.toolbar)
        setSupportActionBar(toolbar)

        checkBackup()
    }

    private fun checkBackup() {
        // Did we check already today? If yes, don't bother asking again
        val dateDiff = Date().time - preferences.getLong("LAST_BACKUP_CHECK", 0L)
        if (TimeUnit.MILLISECONDS.toDays(dateDiff) < 1L) return

        val editor = preferences.edit()
        editor.putLong("LAST_BACKUP_CHECK", Date().time)
        editor.apply()

        val lastBackup = preferences.getLong("LAST_BACKUP", 0L)
        val backupAgeMS = Date().time - lastBackup
        val backupAgeDays = TimeUnit.DAYS.convert(backupAgeMS, TimeUnit.MILLISECONDS)

        // Backup warning
        var backupWarningDays = 0
        // Check if backupWarning is enabled
        val backupWarning = preferences.getString(KEY_PREF_BACKUP_WARNING, BACKUP_WARNING_DEFAULT)
        if (!backupWarning.isNullOrEmpty()) {
            backupWarningDays = try { backupWarning.toInt() }
                                catch  (e: NumberFormatException) { BACKUP_WARNING_DEFAULT.toInt() }
            if (backupAgeDays >= backupWarningDays) userOutputService.showMessageAndWaitForLong(getString(string.backupOverdue))
        }

        // Auto Backup
        // Check if autoBackup is enabled
        val autoBackup = preferences.getString(KEY_PREF_AUTO_BACKUP, AUTO_BACKUP_DEFAULT)
        if (autoBackup.isNullOrEmpty()) return

        val autoBackupDays = try { autoBackup.toInt() }
                         catch  (e: NumberFormatException) { AUTO_BACKUP_DEFAULT.toInt() }

        if (backupAgeDays >= autoBackupDays) {
            // Check if everything is in place for auto backups
            val uriString = preferences.getString(SettingsActivity.KEY_PREF_BACKUP_URI, "")
            if (uriString.isNullOrEmpty()) {
                userOutputService.showMessageAndWaitForLong(getString(string.missingBackupLocation))
                return
            }

            // Valid location?
            val uri = Uri.parse(uriString)
            val dFolder = DocumentFile.fromTreeUri(this, uri)
            if (dFolder == null) {
                userOutputService.showMessageAndWaitForLong(getString(string.invalidBackupLocation))
                return
            }

            // Password
            var zipPassword = preferences.getString(SettingsActivity.KEY_PREF_PASSWORD, "")
            if (zipPassword.isNullOrEmpty()) zipPassword = ""


//            userOutputService.showMessageAndWaitForLong(getString(string.autoBackupStart))
            viewModels[0].writeBackup(uri, zipPassword, true)

            val editor = preferences.edit()
            editor.putLong("LAST_BACKUP_CHECK", Date().time)
            editor.apply()
        }
    }


    private fun initializeServices() {
        userOutputService = UserOutputServiceImpl(applicationContext, findViewById(android.R.id.content))
    }

    override fun onStart() {
        super.onStart()
        checkTimeout()
    }

    private fun checkTimeout() {
        if (!preferences.getBoolean(SettingsActivity.KEY_PREF_BIOMETRIC, false)) {
            mediLog.setAuthenticated(true)
            return
        }

        // Check timer
        val now = Calendar.getInstance().timeInMillis
        val threshold = preferences.getLong("FORCE_REAUTHENTICATION", 300000) // 5 minutes
        val then = preferences.getLong("STOPWATCH", 0L)
        val diff = now - then

        // First check if authentication is enabled
        // If yes, check if re-authentication needs to be forced = if more than threshold milliseconds passed after last onStop event was executed
        if (diff > threshold) {
            mediLog.setAuthenticated(false)
        }

        val biometricHelper = BiometricHelper(this)
        val canAuthenticate = biometricHelper.canAuthenticate(true)
        if (!mediLog.isAuthenticated() && canAuthenticate == 0) {
            // No idea why I need this
            val newExecutor: Executor = Executors.newSingleThreadExecutor()
            val activity: FragmentActivity = this
            val myBiometricPrompt = BiometricPrompt(activity, newExecutor, object : BiometricPrompt.AuthenticationCallback() {

                override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                    super.onAuthenticationError(errorCode, errString)
                    userOutputService.showMessageAndWaitForDurationAndAction(
                            "$errString " + getString(string.retryActionText),
                            300000,
                            getString(string.retryAction),
                            { onStart() },
                            Color.RED)
                }

                override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                    super.onAuthenticationSucceeded(result)
                    mediLog.setAuthenticated(true)
                    runOnUiThread { binding.viewPager.visibility = View.VISIBLE }
                }

            })
            runOnUiThread { binding.viewPager.visibility = View.INVISIBLE }
            if (Build.VERSION.SDK_INT > 29) {
                val promptInfo = PromptInfo.Builder()
                        .setTitle(getString(string.appName))
                        .setDeviceCredentialAllowed(true)  // Allow to use pin as well
                        .setSubtitle(getString(string.biometricLogin))
                        .setConfirmationRequired(false)
                        .build()
                myBiometricPrompt.authenticate(promptInfo)
            } else {
                // Pin fallback Disabled for now until bug https://issuetracker.google.com/issues/142740104 is fixed)
                val promptInfo = PromptInfo.Builder()
                        .setTitle(getString(string.appName))
                        .setNegativeButtonText(getString(string.cancel))
                        .setSubtitle(getString(string.biometricLogin)) //                        .setDescription(getString(R.string.biometricLogin)
                        .setConfirmationRequired(false)
                        .build()
                myBiometricPrompt.authenticate(promptInfo)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)

        val filterActive: Boolean

        val selectedTabPosition = binding.tabLayout.selectedTabPosition
        if (selectedTabPosition >= 0) {
            binding.viewPager.currentItem = selectedTabPosition

            filterActive = doMustActiveFilterByUsing(selectedTabPosition)
        } else {
            binding.viewPager.currentItem = 0
            filterActive = doMustActiveFilterByUsing(0)
        }

        val filterIcon = menu.findItem(id.action_setFilter).icon
        filterIcon.mutate()
        if (filterActive) {
            filterIcon.setTint(getColor(this, color.colorActiveFilter))
        }

        // Disable certain menus if no active Tab exists
        if (hideMenus) {
            menu.findItem(id.action_dataManagement).isVisible = false
            menu.findItem(id.actionPDF).isVisible = false
        }
        /*        else {
            menu.findItem(R.id.action_dataManagement).setVisible(true);
            menu.findItem(R.id.action_send).setVisible(false);
       }*/
        return true
    }

    private fun doMustActiveFilterByUsing(selectedTabPosition: Int) = (
            viewModels.isNotEmpty() && (viewModels[selectedTabPosition].filterStart + viewModels[selectedTabPosition].filterEnd) > 0L
            )

    // Menue
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        val activeTab = binding.tabLayout.selectedTabPosition
        when (id) {
            R.id.action_setFilter -> {
                val bundle = Bundle()

                val vm = viewModels[binding.tabLayout.selectedTabPosition]
                bundle.putInt("activeTab", activeTab)

                if (vm.getSize(false) == 0) {
                    userOutputService.showMessageAndWaitForLong(getString(string.emptyTable))
                    return true
                }

                val dialogFragment = FilterFragment()
                dialogFragment.arguments = bundle

                val ft = supportFragmentManager.beginTransaction()
                val prev: Fragment? = supportFragmentManager.findFragmentByTag("FilterFragment")
                if (prev != null) ft.remove(prev)
                ft.addToBackStack(null)
                dialogFragment.show(ft, "FilterFragment")

                return true
            }

            R.id.action_settings -> {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
                return true
            }

            R.id.actionSendZIP -> {
                val zipPassword = preferences.getString(SettingsActivity.KEY_PREF_PASSWORD, "")
                if (zipPassword.isNullOrEmpty()) getSendZipPassword()
                else sendFile(ZIP_INTENT, zipPassword)
                return true
            }
            R.id.actionSendPDF -> {
                sendFile(PDF_INTENT)
                return true
            }
            R.id.actionOpenPDF -> {
                openFile(PDF_INTENT)
                return true
            }
            R.id.action_restore -> {
                // Need to explicitly ask for permission once
                val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                if (!hasPermissions(mContext, *permissions)) {
                    ActivityCompat.requestPermissions((mContext as Activity), permissions, REQUEST)
                }

                // Pick backup folder
                var chooseFile = Intent(Intent.ACTION_GET_CONTENT)
                chooseFile.type = "*/*"
                chooseFile = Intent.createChooser(chooseFile, this.getString(string.selectFile))
                startActivityForResult(chooseFile, rcRESTORE)
                return true
            }
            R.id.action_backup -> {
                // Need to explicitly ask for permission once
                val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                if (!hasPermissions(mContext, *permissions)) {
                    ActivityCompat.requestPermissions((mContext as Activity), permissions, REQUEST)
                }
//                userOutputService.showMessageAndWaitForLong(this.getString(string.selectDirectory))

                // Pick backup folder
                val intent = Intent(Intent.ACTION_OPEN_DOCUMENT_TREE)
                intent.addCategory(Intent.CATEGORY_DEFAULT)
                intent.addFlags(FLAG_GRANT_PERSISTABLE_URI_PERMISSION)
                intent.addFlags(FLAG_GRANT_READ_URI_PERMISSION)
                intent.addFlags(FLAG_GRANT_WRITE_URI_PERMISSION)
                startActivityForResult(Intent.createChooser(intent, this.getString(string.selectDirectory)), rcBACKUP)
                return true
            }
            R.id.action_about -> {
                val intent = Intent(this, AboutActivity::class.java)
                startActivity(intent)
                return true
            }
/*            R.id.action_editCustomData -> {
                val intent = Intent(this, CustomTabsEditActivity::class.java)
                startActivity(intent)
                return true
            }*/
        }
        return super.onOptionsItemSelected(item)
    }

    private fun openFile(type: String) {
        var uri: Uri? = null
        when (type) {
            PDF_INTENT -> uri = viewModels[binding.tabLayout.selectedTabPosition].getPdfFile(viewModels[binding.tabLayout.selectedTabPosition].createPdfDocument(true))
        }

        if (uri != null) {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.type = type
            intent.data = uri;
            intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION;
            try {
                startActivity(intent);
            } catch (e: Exception) {
                userOutputService.showMessageAndWaitForLong(getString(string.eShareError) + ": " + e.localizedMessage)
            }
        }
    }

    private fun sendFile(type: String, zipPassword: String = "") {
        // Need to explicitly ask for permission once
        val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (!hasPermissions(mContext, *permissions)) {
            ActivityCompat.requestPermissions((mContext as Activity), permissions, REQUEST)
        }

        val intent = Intent(Intent.ACTION_SEND)
        intent.type = type
        intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        intent.putExtra(Intent.EXTRA_TEXT, getString(string.sendText))

        intent.putExtra(Intent.EXTRA_SUBJECT, "MediLog " + getString(string.data))
        val vm = viewModels[binding.tabLayout.selectedTabPosition]
        var uri: Uri? = null
        when (type) {
            ZIP_INTENT -> uri = vm.getZIP(vm.createPdfDocument(true), zipPassword)
            CSV_INTENT -> uri = vm.writeCsvFile(true)
            PDF_INTENT -> uri = vm.getPdfFile(viewModels[binding.tabLayout.selectedTabPosition].createPdfDocument(true))
        }

        if ((uri != null) && (intent.resolveActivity(packageManager) != null)) {
            intent.putExtra(Intent.EXTRA_STREAM, uri)
            try {
                startActivity(Intent.createChooser(intent, getString(string.sendText)))
            } catch (e: Exception) {
                userOutputService.showMessageAndWaitForLong(getString(string.eShareError) + ": " + e.localizedMessage)
            }
        }
    }

    private fun getSendZipPassword() {
        MaterialDialog(this).show {
            title(string.enterPassword)
            input(inputType = InputType.TYPE_CLASS_TEXT or TYPE_TEXT_VARIATION_PASSWORD, allowEmpty = true, hintRes = string.password) { _, text ->
                sendFile(ZIP_INTENT, text.toString())
            }
            positiveButton(string.submit)
            negativeButton(string.cancel)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        data ?: return

        // Backup section
        if (requestCode == rcBACKUP && resultCode == Activity.RESULT_OK) {
            var backupFolderUri: Uri? = null

            // Check Uri is sound
            try {
                backupFolderUri = data.data
            } catch (e: Exception) {
                userOutputService.showMessageAndWaitForLong(this.getString(string.eSelectDirectory) + " " + data)
                e.printStackTrace()
            }
            if (backupFolderUri == null) {
                userOutputService.showMessageAndWaitForLong(this.getString(string.eSelectDirectory) + " " + data)
                return
            }

            val zipPassword = preferences.getString(SettingsActivity.KEY_PREF_PASSWORD, "")
            if (zipPassword.isNullOrEmpty()) {
                MaterialDialog(this).show {
                    title(string.enterPassword)
                    input(inputType = InputType.TYPE_CLASS_TEXT or TYPE_TEXT_VARIATION_PASSWORD, allowEmpty = true, hintRes = string.password) { _, text ->
                        viewModels[binding.tabLayout.selectedTabPosition].writeBackup(backupFolderUri, text.toString())
                    }
                    positiveButton(string.submit)
                    negativeButton(string.cancel)
                }
            } else viewModels[binding.tabLayout.selectedTabPosition].writeBackup(backupFolderUri, zipPassword)
        }

        // Restore from backup
        if (requestCode == rcRESTORE  && resultCode == Activity.RESULT_OK) {
            var backupFileUri: Uri? = null
            try {
                backupFileUri = data.data
            } catch (e: Exception) {
                userOutputService.showMessageAndWaitForLong(this.getString(string.eSelectDirectory) + " " + data)
            }
            if (backupFileUri == null) {
                return
            }

            // Analyse file
            val cr = contentResolver
            val mimeType = cr.getType(backupFileUri)
            val existingRecords = viewModels[binding.tabLayout.selectedTabPosition].getDataTableSize()

            // Check if user needs to be warned
            if (existingRecords > 0) {
                MaterialDialog(this).show {
                    title(R.string.warning)
                    message(text = getString(R.string.doYouReallyWantToContinue1) + " " + existingRecords + " " + getString(R.string.doYouReallyWantToContinue2) + " " + getString(R.string.doYouReallyWantToContinue3))
                    negativeButton(R.string.cancel)
                    positiveButton(R.string.yes) {
                        when (mimeType) {
                            ZIP_INTENT -> {
                                val zipPassword = preferences.getString(SettingsActivity.KEY_PREF_PASSWORD, "")
                                if (zipPassword.isNullOrEmpty()) getOpenZipPassword(backupFileUri)
                                else viewModels[binding.tabLayout.selectedTabPosition].readBackup(backupFileUri, zipPassword)
                            }
                            "application/csv",
                            "text/csv",
                            "text/comma-separated-values" -> {
                                viewModels[binding.tabLayout.selectedTabPosition].readBackup(backupFileUri, "csv")
                            }
                            else -> userOutputService.showMessageAndWaitForLong("Don't know how to handle file type: $mimeType")
                        }
                    }
                }
            }
            else {
                when (mimeType) {
                    ZIP_INTENT -> {
                        val zipPassword = preferences.getString(SettingsActivity.KEY_PREF_PASSWORD, "")
                        if (zipPassword.isNullOrEmpty()) getOpenZipPassword(backupFileUri)
                        else viewModels[binding.tabLayout.selectedTabPosition].readBackup(backupFileUri, zipPassword)
                        return
                    }
                    "application/csv",
                    "text/csv",
                    "text/comma-separated-values" -> {
                        viewModels[binding.tabLayout.selectedTabPosition].readBackup(backupFileUri, "csv")
                        return
                    }
                    else -> {
                        userOutputService.showMessageAndWaitForLong("Don't know how to handle file type: $mimeType")
                        return
                    }
                }
            }
        }
    }

    private fun getOpenZipPassword(zipUri: Uri) {
        MaterialDialog(this).show {
            title(string.enterPassword)
            input(inputType = InputType.TYPE_CLASS_TEXT or TYPE_TEXT_VARIATION_PASSWORD, allowEmpty = true, hintRes = string.password) { _, text ->
                viewModels[binding.tabLayout.selectedTabPosition].readBackup(zipUri, text.toString())
            }
            positiveButton(string.submit)
            negativeButton(string.cancel)
        }
    }

    private fun loadActiveTabs() {
        // Load activeTabs
        val serializedArray = preferences.getString("ACTIVE_TABS", "")

        activeTabs.clear()
        if (serializedArray.isNullOrEmpty()) {
            // Activate and save the default tabs we ship with
            activeTabs.add(getString(string.tab_weight))
            activeTabs.add(getString(string.tab_bloodpressure))
            activeTabs.add(getString(string.tab_bloodsugar))
            activeTabs.add(getString(string.tab_water))
            activeTabs.add(getString(string.tab_diary))

            // Store right away to make sure preferences and activeTabs are in sync
            val tabsAsString = arrayToString(activeTabs)
            val editor = preferences.edit()
            editor.putString("ACTIVE_TABS", tabsAsString)
            editor.apply()
        } else {
            val arr = serializedArray.split(",")
            for (tab in arr) {
                activeTabs.add(tab)
            }
        }

        // Check that activeTabs matches the hashSet
        val hashSet = preferences.getStringSet(SettingsActivity.KEY_PREF_ACTIVE_TABS_SET, HashSet())

        // hashSet == null shouldn't happen
        // hasSet.size == 0 happens only if the tab selecting tab was never opened yet in which case we go with the default = all Tabs active
        if (hashSet == null || hashSet.size == 0) {
            return
        }
        // Temporary array
        val newActiveTabs = ArrayList<String>()
        for (tab in activeTabs) newActiveTabs.add(tab)

        // Sync hashSet and activeTabs without losing the order in activeTabs
        // Remove the tabs not available in hashSet = Disabled by the user in the settings dialog
        for (tab in activeTabs) {
            if (!hashSet.contains(tab)) newActiveTabs.remove(tab)
        }
        // Add the tabs not in activeTabs but in hashSet = Added by the user in settings dialog
        for (tab in hashSet) {
            if (!activeTabs.contains(tab)) newActiveTabs.add(tab)
        }
//newActiveTabs.removeAt(3)

        // Store right away to make sure preferences and activeTabs are in sync
        val tabsAsString = arrayToString(newActiveTabs)
        val editor = preferences.edit()
        editor.putString("ACTIVE_TABS", tabsAsString)
        editor.apply()

        activeTabs = newActiveTabs
    }

    // Take a , separated string array and return a single string
    private fun arrayToString(tabs: ArrayList<String>): String {
        val sb = StringBuilder()
        for (tab in tabs) sb.append(tab).append(",")
        var serialized = sb.toString()
        if (serialized.isNotEmpty()) serialized = serialized.substring(0, serialized.length - 1) // Remove trailing ,
        return serialized
    }

    private fun reorderTabs(dragSource: Int, dragTarget: Int) {
        if (dragSource == dragTarget) return

        val newOrder = ArrayList<String>()

        if (!activeTabs.isNullOrEmpty()) {
            val keepTab = activeTabs[dragSource]
            val afterTab = activeTabs[dragTarget]
            activeTabs.removeAt(dragSource)
            for (tab in activeTabs) {
                newOrder.add(tab)
                if (tab == afterTab) newOrder.add(keepTab)
            }

            activeTabs = newOrder

            val sb = StringBuilder()
            for (tab in activeTabs) {
                sb.append(tab).append(",")
            }

            var tmp = sb.toString()
            tmp = tmp.substring(0, tmp.length - 1) // Remove trailing ,
            val editor = preferences.edit()
            editor.putString("ACTIVE_TABS", tmp)
            editor.apply()

            this.recreate()
        }
    }

    // #####################################################
// Application independent code
// #####################################################
    public override fun onPause() {
        super.onPause()
        // Save active tab
        val editor = preferences.edit()
        editor.putInt("activeTab", binding.tabLayout.selectedTabPosition)
        editor.apply()
    }

    public override fun onStop() {
        super.onStop()
        // Save active tab
        val editor = preferences.edit()
        editor.putLong("STOPWATCH", Calendar.getInstance().timeInMillis) // Set Re-authentication timer
        editor.apply()
    }

    private fun setFilterIcon() {
        invalidateOptionsMenu()
    }

    companion object {
        var activeTabs = ArrayList<String>()
        var themeColour: String? = null

        const val DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss"
        const val DATE_PATTERN = "yyyy-MM-dd"

        // Constants to be used with settings viewModel
        const val MEDILOG = 0
        const val WEIGHT = 1
        const val BLOODPRESSURE = 2
        const val DIARY = 3
        const val WATER = 4
        const val BLOODSUGAR = 5
        const val FIRST_CUSTOM = 6

        const val TEXT_SIZE_DEFAULT = "15"
        const val MIN_TEXT_SIZE = "10"
        const val MAX_TEXT_SIZE = "40"

        const val BLOODPRESSURE_UNIT_DEFAULT = "mmHg"
        const val SHOW_BLOODPRESURE_THRESHOLD_DEFAULT = true

        const val BLOODSUGAR_UNIT_DEFAULT = "mg/dL"
        const val BLOODSUGAR_LOWER_THRESHOLD_DEFAULT = "70"
        const val BLOODSUGAR_UPPER_THRESHOLD_DEFAULT = "130"

        const val BACKUP_WARNING_DEFAULT = "7"
        const val AUTO_BACKUP_DEFAULT = ""

        const val ZIP_INTENT = "application/zip"
        const val CSV_INTENT = "application/txt"
        const val PDF_INTENT = "application/pdf"

        // Tab Preferences Defaults
        const val SHOW_TAB_TEXT_DEFAULT = false
        const val SHOW_TAB_ICON_DEFAULT = true
        const val SCROLL_TABS_DEFAULT = false

        // Weight Preferences Defaults
        const val WEIGHT_UNIT_DEFAULT = "kg"
        const val WEIGHT_THRESHOLD_DEFAULT = "80"
        const val SHOW_WEIGHT_THRESHOLD_DEFAULT = true
        const val SHOW_WEIGHT_GRID_DEFAULT = false
        const val SHOW_WEIGHT_LEGEND_DEFAULT = false
        const val WEIGHT_LINEAR_TRENDLINE_DEFAULT = false
        const val WEIGHT_MOVING_AVERAGE_TRENDLINE_DEFAULT = false
        const val WEIGHT_MOVING_AVERAGE_SIZE_DEFAULT = "6"
        const val KEY_PREF_WEIGHT_DAY_STEPPING_DEFAULT = false
        const val KEY_PREF_WEIGHT_BAR_CHART_DEFAULT = false

        // Water Preferences Defaults
        const val WATER_UNIT_DEFAULT = "ml"
        const val WATER_THRESHOLD_DEFAULT = "2000"
        const val SHOW_WATER_THRESHOLD_DEFAULT = true
        const val WATER_SHOW_PDF_SUMMARY_DEFAULT = true


        const val tmpComment = "temporaryEntry-YouShouldNeverSeeThis"

        var viewModels = ArrayList<ViewModel>()

        private const val REQUEST = 112
        private fun hasPermissions(context: Context?, vararg permissions: String): Boolean {
            if (context != null) {
                for (permission in permissions) {
                    if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                        return false
                    }
                }
            }
            return true
        }

        fun setTheme(context: Context?) {
            if (context == null) return

            when (themeColour) {
                context.getString(string.green) -> context.theme.applyStyle(style.AppThemeGreenBar, true)
                context.getString(string.red) -> context.theme.applyStyle(style.AppThemeRedBar, true)
                context.getString(string.gray) -> context.theme.applyStyle(style.AppThemeGrayBar, true)
                else -> context.theme.applyStyle(style.AppThemeBlueBar, true)
            }
        }

        fun resetReAuthenticationTimer(context: Context?) {
            // Reset Stopwatch, after all we are still in MediLog
            val preferences = Preferences.getSharedPreferences(context!!)
            val editor = preferences.edit()
            editor.putLong("STOPWATCH", Calendar.getInstance().timeInMillis) // Set Re-authentication timer
            editor.apply()
        }

    }
}
