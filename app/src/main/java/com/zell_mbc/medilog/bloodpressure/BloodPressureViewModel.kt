package com.zell_mbc.medilog.bloodpressure

import android.app.Application
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.pdf.PdfDocument
import com.zell_mbc.medilog.*
import com.zell_mbc.medilog.MainActivity.Companion.BLOODPRESSURE
import com.zell_mbc.medilog.data.ViewModel
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity
import java.text.DateFormat
import java.util.*

class BloodPressureViewModel(application: Application): ViewModel(application) {
    override var tabIcon  = R.drawable.ic_bloodpressure
    override val filterStartPref = "BLOODPRESSUREFILTERSTART"
    override val filterEndPref = "BLOODPRESSUREFILTEREND"
    override var itemName = app.getString(R.string.bloodPressure)
    private var userOutputService: UserOutputService = UserOutputServiceImpl(app,null)

    override fun createPdfDocument(filtered: Boolean): PdfDocument? {
        if (getSize(filtered) == 0) {
            userOutputService.showAndHideMessageForLong(app.getString(R.string.bloodPressure) + " " + app.getString(R.string.noDataToExport))
            return null
        }

        val userName = preferences.getString(SettingsActivity.KEY_PREF_USER, "")
        val logHeartRhythm = preferences.getBoolean(SettingsActivity.KEY_PREF_LOG_HEART_RHYTHM, true)
        val highlightValues = preferences.getBoolean(SettingsActivity.KEY_PREF_COLOUR, false)
        val bpHelper = BloodPressureHelper(app)

        val document = PdfDocument()
        val diaTab = 30
        val stateTab = 125
        val section1 = 140
        val section2 = 195
        val section3 = 260
        val pulseTab = 320
        val commentTab = 360
        val pn = 1
        var i: Int
        var pageInfo: PdfDocument.PageInfo?
        var page: PdfDocument.Page
        var canvas: Canvas
        val formatedDate = DateFormat.getDateInstance(DateFormat.SHORT).format(Calendar.getInstance().time) + " - " + DateFormat.getTimeInstance(DateFormat.SHORT).format(Calendar.getInstance().time)

        val pdfPaint = Paint()
        pdfPaint.isFakeBoldText = false
        pdfPaint.color = Color.BLACK

        val pdfPaintHighlight = Paint()
        pdfPaintHighlight.isFakeBoldText = true

        val paintRed = Paint()
        paintRed.color = Color.RED

        // Black & White instead of colour
        val blackAndWhite = true

        // -----------
        // crate a A4 page description
        pageInfo = PdfDocument.PageInfo.Builder(595, 842, pn).create()
        page = document.startPage(pageInfo)
        canvas = page.canvas

        pdfRightBorder = canvas.width - pdfLeftBorder
        pdfDataBottom = canvas.height - 15

        val pdfHeaderDateColumn = pdfRightBorder - 150

        // Draw header
        var headerText = app.getString(R.string.bpReportTitle)
        if (userName!!.isNotEmpty()) {
            headerText = headerText + " " + app.getString(R.string.forString) + " " + userName
        }
        //                Log.d("--------------- Debug", "HeaderText:" + headerText);
        canvas.drawText(headerText, pdfLeftBorder.toFloat(), pdfHeaderTop.toFloat(), pdfPaint)
        canvas.drawText(app.getString(R.string.date) + ": " + formatedDate, pdfHeaderDateColumn.toFloat(), pdfHeaderTop.toFloat(), pdfPaint)
        canvas.drawLine(pdfLeftBorder.toFloat(), pdfHeaderBottom.toFloat(), pdfRightBorder.toFloat(), pdfHeaderBottom.toFloat(), pdfPaint)
        canvas.drawLine(pdfLeftBorder.toFloat(), pdfDataBottom.toFloat(), pdfRightBorder.toFloat(), pdfDataBottom.toFloat(), pdfPaint)

        // Data section
        i = pdfDataTop
        canvas.drawText(app.getString(R.string.date), pdfLeftBorder.toFloat(), i.toFloat(), pdfPaintHighlight)
        if (logHeartRhythm) canvas.drawText("!", stateTab.toFloat(), i.toFloat(), pdfPaintHighlight)
        canvas.drawText(app.getString(R.string.morning), section1.toFloat(), i.toFloat(), pdfPaintHighlight)
        canvas.drawText(app.getString(R.string.afternoon), section2.toFloat(), i.toFloat(), pdfPaintHighlight)
        canvas.drawText(app.getString(R.string.evening), section3.toFloat(), i.toFloat(), pdfPaintHighlight)
        canvas.drawText(app.getString(R.string.pulse), pulseTab.toFloat(), i.toFloat(), pdfPaintHighlight)
        canvas.drawText(app.getString(R.string.comment), commentTab.toFloat(), i.toFloat(), pdfPaintHighlight)
        // ------------
        val space = 5
        pdfPaint.color = Color.DKGRAY
        canvas.drawLine(section1 - space.toFloat(), pdfHeaderBottom.toFloat(), section1 - space.toFloat(), pdfDataBottom.toFloat(), pdfPaint)
        canvas.drawLine(section2 - space.toFloat(), pdfHeaderBottom.toFloat(), section2 - space.toFloat(), pdfDataBottom.toFloat(), pdfPaint)
        canvas.drawLine(section3 - space.toFloat(), pdfHeaderBottom.toFloat(), section3 - space.toFloat(), pdfDataBottom.toFloat(), pdfPaint)
        pdfPaint.color = Color.BLACK
        canvas.drawLine(pulseTab - space.toFloat(), pdfHeaderBottom.toFloat(), pulseTab - space.toFloat(), pdfDataBottom.toFloat(), pdfPaint)
        canvas.drawLine(commentTab - space.toFloat(), pdfHeaderBottom.toFloat(), commentTab - space.toFloat(), pdfDataBottom.toFloat(), pdfPaint)

        val items = getItems("ASC", filtered)
        for (item in items) {
            i += pdfLineSpacing
            // Start new page
            if (i > pdfDataBottom) {
                document.finishPage(page)

                // -----------
                // crate a A4 page description
                pageInfo = PdfDocument.PageInfo.Builder(595, 842, pn).create()
                page = document.startPage(pageInfo)
                canvas = page.canvas
//                dataBottom = canvas.height - 15
                canvas.drawText(headerText, pdfLeftBorder.toFloat(), pdfHeaderBottom.toFloat(), pdfPaint)
                i = pdfDataTop
                canvas.drawText(app.getString(R.string.date), pdfLeftBorder.toFloat(), i.toFloat(), pdfPaint)
                //               canvas.drawText(app.getString(R.string.time), timeTab, i, paint);
                canvas.drawText(app.getString(R.string.morning), section1.toFloat(), i.toFloat(), pdfPaint)
                canvas.drawText(app.getString(R.string.afternoon), section2.toFloat(), i.toFloat(), pdfPaint)
                canvas.drawText(app.getString(R.string.evening), section3.toFloat(), i.toFloat(), pdfPaint)
                canvas.drawText(app.getString(R.string.pulse), pulseTab.toFloat(), i.toFloat(), pdfPaint)
                canvas.drawText(app.getString(R.string.comment), commentTab.toFloat(), i.toFloat(), pdfPaint)
                // ------------
                i += pdfLineSpacing
            }
            canvas.drawText(toStringDate(item.timestamp), pdfLeftBorder.toFloat(), i.toFloat(), pdfPaint)
            canvas.drawText(toStringTime(item.timestamp), pdfTimeTab.toFloat(), i.toFloat(), pdfPaint)

            // Todo: Replace ! with bitmap for state
//            val bm = BitmapFactory.decodeResource(app.resources,R.id.ivHeartRhythmIssue)
            if (logHeartRhythm && item.value4 == "1") canvas.drawText( "!", stateTab.toFloat(), i.toFloat(), pdfPaint)

            val dayPeriod = dayPeriod(item.timestamp)
            var activeSection = section1
            if (dayPeriod == 1) {
                activeSection = section2
            }
            if (dayPeriod == 2) {
                activeSection = section3
            }

            if (highlightValues) {
                if (blackAndWhite) {
                    when(bpHelper.sysGrade(item.value1)) {
                        bpHelper.hyperGrade3,
                        bpHelper.hyperGrade2 -> {
                            pdfPaint.isFakeBoldText = true
                            pdfPaint.isUnderlineText = true
                        }
                        bpHelper.hyperGrade1 -> pdfPaint.isFakeBoldText = true
                        else                 -> pdfPaint.isFakeBoldText = false
                    }
                    canvas.drawText(item.value1, activeSection.toFloat(), i.toFloat(), pdfPaint)
                    pdfPaint.isFakeBoldText = false
                    pdfPaint.isUnderlineText = false

                    when(bpHelper.diaGrade(item.value2)) {
                        bpHelper.hyperGrade3,
                        bpHelper.hyperGrade2 -> {
                            pdfPaint.isFakeBoldText = true
                            pdfPaint.isUnderlineText = true
                        }
                        bpHelper.hyperGrade1 -> pdfPaint.isFakeBoldText = true
                        else                 -> pdfPaint.isFakeBoldText = false
                    }
                    canvas.drawText(item.value2, activeSection + diaTab.toFloat(), i.toFloat(), pdfPaint)
                    pdfPaint.isFakeBoldText = false
                    pdfPaint.isUnderlineText = false
                } // blackAndWhite
                else {
                    when(bpHelper.sysGrade(item.value1)) {
                        bpHelper.hyperGrade3 -> pdfPaint.color = Color.RED
                        bpHelper.hyperGrade2 -> pdfPaint.color = Color.RED
                        bpHelper.hyperGrade1 -> pdfPaint.color = Color.rgb(255, 165, 0)
                        else                 -> pdfPaint.color = Color.BLACK
                    }

                    when(bpHelper.diaGrade(item.value2)) {
                        bpHelper.hyperGrade3 -> pdfPaint.color = Color.RED
                        bpHelper.hyperGrade2 -> pdfPaint.color = Color.RED
                        bpHelper.hyperGrade1 -> pdfPaint.color = Color.rgb(255, 165, 0)
                        else                    -> pdfPaint.color = Color.BLACK
                    }
                    canvas.drawText(item.value2, activeSection + diaTab.toFloat(), i.toFloat(), pdfPaint)
                    pdfPaint.color = Color.BLACK
                }
            }
            else {  // Don't highlight values
                canvas.drawText(item.value1,    activeSection.toFloat(), i.toFloat(), pdfPaint)
                canvas.drawText(item.value2, activeSection + diaTab.toFloat(), i.toFloat(), pdfPaint)
            }
            canvas.drawText(item.value3, pulseTab.toFloat(), i.toFloat(), pdfPaint)
            canvas.drawText(item.comment, commentTab.toFloat(), i.toFloat(), pdfPaint)
        }
        // finish the page
        document.finishPage(page)

        //      FileHelper fh = new FileHelper(app, fileName);
        return document
    }

    private fun dayPeriod(timestamp: Long): Int {
        val p: Int
        val cal = Calendar.getInstance()
        cal.timeInMillis = timestamp
        val hour = cal[Calendar.HOUR_OF_DAY]
        p = when {
            hour <= 11 -> {
                0
            } // Morning
            hour <= 17 -> {
                1
            } // Afternoon
            else -> {
                2
            }
        } // Evening
        return p
    }
}
