package com.zell_mbc.medilog.bloodpressure

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Typeface
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utillity.Preferences
import java.text.DateFormat

class BloodPressureListAdapter internal constructor(context: Context) : RecyclerView.Adapter<BloodPressureListAdapter.BloodPressureViewHolder>() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var clickListener: ItemClickListener? = null

    private var items = emptyList<Data>()
    private val textSize: Float
    private var dateFormat: DateFormat
    private var timeFormat: DateFormat
    private val highlightValues: Boolean
    private var bpHelper: BloodPressureHelper
    private val logHeartRhythm: Boolean


    inner class BloodPressureViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val ivHeartRhythm: ImageView = itemView.findViewById(R.id.ivHeartRhythmIssue)
        val tvDate: TextView = itemView.findViewById(R.id.tvBloodPressureDate)
        val tvSys: TextView = itemView.findViewById(R.id.tvBloodPressureSys)
        val tvDia: TextView = itemView.findViewById(R.id.tvBloodPressureDia)
        val tvPulse: TextView = itemView.findViewById(R.id.tvBloodPressurePulse)
        val tvComment: TextView = itemView.findViewById(R.id.tvBloodPressureComment)

        override fun onClick(view: View) {
            if (clickListener != null) clickListener!!.onItemClick(view, adapterPosition)
        }

        init {
            tvDate.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
            tvSys.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
            tvDia.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
            tvPulse.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
            tvComment.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
            itemView.setOnClickListener(this)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BloodPressureListAdapter.BloodPressureViewHolder {
        val itemView = inflater.inflate(R.layout.bloodpressureview_row, parent, false)
        return BloodPressureViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: BloodPressureListAdapter.BloodPressureViewHolder, position: Int) {
        val current = items[position]

        if (logHeartRhythm) {
            // Check value
            var heartRhythm = -1
            if (current.value4.isNotEmpty()) {
                heartRhythm = try { current.value4.toInt() }
                              catch (e: NumberFormatException) { 0 }
            }

            // Show image only if value > 0
            if (heartRhythm > 0) holder.ivHeartRhythm.visibility = View.VISIBLE  //                    holder.ivHeartRhythm.imageTintList = ColorStateList.valueOf(Color.rgb(255, 0, 0))
            else holder.ivHeartRhythm.visibility = View.INVISIBLE
        }
        else holder.ivHeartRhythm.visibility = View.GONE         // Switch off view to regain space

        val tmpString = dateFormat.format(current.timestamp) + " - " + timeFormat.format(current.timestamp)
        holder.tvDate.text = tmpString

        if (highlightValues) {
            val orgColour = holder.tvDate.textColors.defaultColor
            when (bpHelper.sysGrade(current.value1)) {
                bpHelper.hyperGrade3 -> {
                    holder.tvSys.setTextColor(Color.rgb(255, 0, 0))
                    holder.tvSys.setTypeface(null, Typeface.BOLD)
                }
                bpHelper.hyperGrade2 -> holder.tvSys.setTextColor(Color.rgb(255, 0, 0))
                bpHelper.hyperGrade1 -> holder.tvSys.setTextColor(Color.rgb(255, 165, 0))
                else ->                 holder.tvSys.setTextColor(orgColour)
            }

            holder.tvSys.text = current.value1

            when (bpHelper.diaGrade(current.value2)) {
                bpHelper.hyperGrade3 -> {
                    holder.tvDia.setTextColor(Color.rgb(255, 0, 0))
                    holder.tvDia.setTypeface(null, Typeface.BOLD)
                }
                bpHelper.hyperGrade2 -> holder.tvDia.setTextColor(Color.rgb(255, 0, 0))
                bpHelper.hyperGrade1 -> holder.tvDia.setTextColor(Color.rgb(255, 165, 0))
                else -> holder.tvDia.setTextColor(orgColour)
            }
            holder.tvDia.text = current.value2

            }
        else {
            holder.tvSys.text = current.value1
            holder.tvDia.text = current.value2
            }
        holder.tvPulse.text = current.value3
        holder.tvComment.text = current.comment
    }

    internal fun setItems(bloodPressures: List<Data>) {
        this.items = bloodPressures
        notifyDataSetChanged()
    }

    fun getItemAt(position: Int): Data {
        return items[position]
    }

    // allows clicks events to be caught
    fun setClickListener(itemClickListener: ItemClickListener?) {
        clickListener = itemClickListener
    }


    // parent activity will implement this method to respond to click events
    interface ItemClickListener {
        fun onItemClick(view: View?, position: Int)
    }

    override fun getItemCount() = items.size

    init {
        val preferences = Preferences.getSharedPreferences(context)
        dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
        timeFormat = DateFormat.getTimeInstance(DateFormat.SHORT)
        textSize = java.lang.Float.valueOf(preferences.getString(SettingsActivity.KEY_PREF_TEXT_SIZE, "15")!!)
        highlightValues = preferences.getBoolean(SettingsActivity.KEY_PREF_COLOUR, false)
        logHeartRhythm = preferences.getBoolean(SettingsActivity.KEY_PREF_LOG_HEART_RHYTHM, true)
        bpHelper = BloodPressureHelper(context)
    }
}