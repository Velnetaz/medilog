package com.zell_mbc.medilog.water

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.WATER
import com.zell_mbc.medilog.MainActivity.Companion.WATER_UNIT_DEFAULT
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.databinding.WatereditformBinding
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utillity.Preferences
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class WaterEditFragment : Fragment() {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: WatereditformBinding? = null
    private val binding get() = _binding!!
    private lateinit var userOutputService : UserOutputService


    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = WatereditformBinding.inflate(inflater,container,false)
        initializeService(binding.root)
        return binding.root
    }
    private fun initializeService(view: View) {
        userOutputService = UserOutputServiceImpl(requireContext(),view)
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    lateinit var viewModel: WaterViewModel // by viewModels() //factoryProducer = { SavedStateViewModelFactory(this, ) })
    private val timestampCal = Calendar.getInstance()

    var itemID: Int = 0

    @SuppressLint("SimpleDateFormat")
    private fun saveItem() {
        val editItem = viewModel.getItem(itemID)
        if (editItem == null) {
            userOutputService.showMessageAndWaitForLong("Unknown error!")
            return
        }

        // Check empty variables
        val value = binding.etEditWater.text.toString()
        if (value.isEmpty()) {
            userOutputService.showMessageAndWaitForLong(this.getString(R.string.waterMissing))
            return
        }

        // Valid water?
        var waterValue = 0
        try {
            waterValue = value.toInt()
            if (waterValue <= 0) {
                userOutputService.showMessageAndWaitForLong(this.getString(R.string.invalid) + " " + this.getString(R.string.water) + " "+ this.getString(R.string.value) + " $waterValue")
                return
            }
        } catch (e: Exception) {
            userOutputService.showMessageAndWaitForLong("Exception: " + this.getString(R.string.invalid) + " " + this.getString(R.string.water) + " "+ this.getString(R.string.value) + " $waterValue")
            return
        }

        editItem.timestamp = timestampCal.timeInMillis
        editItem.value1 = binding.etEditWater.text.toString()
        editItem.comment = binding.etComment.text.toString()
        editItem.value2 = SimpleDateFormat("yyyyMMdd").format(editItem.timestamp)

        viewModel.update(editItem)

        userOutputService.showMessageAndWaitForLong(requireContext().getString(R.string.itemUpdated))
        requireActivity().onBackPressed()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(requireActivity()).get(WaterViewModel::class.java)
        viewModel.init(WATER)

        val sharedPref = Preferences.getSharedPreferences(this.requireContext())
        val colourStyle = sharedPref.getString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(R.string.blue))
        val saveButton = binding.btSave

        when (colourStyle) {
            this.getString(R.string.green) -> binding.btSave.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryGreen))
            this.getString(R.string.red)   -> binding.btSave.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryRed))
            this.getString(R.string.gray)   -> binding.btSave.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryGray))
            else   -> binding.btSave.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryBlue))
        }
        saveButton.setColorFilter(Color.WHITE)

        binding.tvEditUnit.text = sharedPref.getString(SettingsActivity.KEY_PREF_WATER_UNIT, WATER_UNIT_DEFAULT)

        val editItem = viewModel.getItem(itemID)
        if (editItem == null) {
            userOutputService.showMessageAndWaitForLong("Unknown error!")
            return
        }

        binding.etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(editItem.timestamp)
        binding.etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(editItem.timestamp)

        // Check if we are really editing or if this is a new value, if new entry delete tmpComment
        if (viewModel.isTmpItem(editItem.comment))
            binding.etComment.setText("")
        else {
            binding.etEditWater.setText(editItem.value1)
            binding.etComment.setText(editItem.comment)
        }

        // Respond to click events
        saveButton.setOnClickListener { saveItem() }

        // ---------------------
        // Date/Time picker section
        timestampCal.timeInMillis = editItem.timestamp

        // create an OnDateSetListener
        val dateListener = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            timestampCal.set(Calendar.YEAR, year)
            timestampCal.set(Calendar.MONTH, monthOfYear)
            timestampCal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            binding.etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
            binding.etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
        }

        val timeListener = TimePickerDialog.OnTimeSetListener { _, hour, minute ->
            timestampCal.set(Calendar.HOUR_OF_DAY, hour)
            timestampCal.set(Calendar.MINUTE, minute)
            binding.etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
            binding.etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
        }

        binding.etDate.setOnClickListener {
            DatePickerDialog(requireContext(),
                    dateListener,
                    timestampCal.get(Calendar.YEAR),
                    timestampCal.get(Calendar.MONTH),
                    timestampCal.get(Calendar.DAY_OF_MONTH)).show()
        }

        binding.etTime.setOnClickListener {
            TimePickerDialog(requireContext(),
                    timeListener,
                    timestampCal.get(Calendar.HOUR_OF_DAY),
                    timestampCal.get(Calendar.MINUTE),
                    android.text.format.DateFormat.is24HourFormat(requireContext())).show()
        }

        // Make sure first field is highlighted and keyboard is open
        binding.etEditWater.requestFocus()

    }

    override fun onPause() {
        super.onPause()
        MainActivity. resetReAuthenticationTimer(requireContext())
        viewModel.deleteTmpItem() // Deletes all items with comment == tmpComment
    }

    fun newInstance(i: Int): WaterEditFragment {
        val f = WaterEditFragment()
        // Supply index input as an argument.
        f.itemID = i
        return f
    }
}

