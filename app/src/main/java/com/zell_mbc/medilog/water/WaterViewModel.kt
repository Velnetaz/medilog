package com.zell_mbc.medilog.water

import android.annotation.SuppressLint
import android.app.Application
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.pdf.PdfDocument
import androidx.lifecycle.*
import com.zell_mbc.medilog.MainActivity.Companion.WATER
import com.zell_mbc.medilog.MainActivity.Companion.WATER_SHOW_PDF_SUMMARY_DEFAULT
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.MainActivity.Companion.WATER_THRESHOLD_DEFAULT
import com.zell_mbc.medilog.MainActivity.Companion.WATER_UNIT_DEFAULT
import kotlinx.coroutines.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import com.zell_mbc.medilog.data.ViewModel
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity


class WaterViewModel(application: Application): ViewModel(application) {
    override var tabIcon  = R.drawable.ic_water
    override val filterStartPref = "WATERFILTERSTART"
    override val filterEndPref = "WATERFILTEREND"
    override var itemName = app.getString(R.string.water)
    private var userOutputService: UserOutputService = UserOutputServiceImpl(app,null)

    lateinit var todaysIntake: LiveData<Int>

    override fun init(dt: Int) {
        super.init(dt)
        todaysIntake = getToday() // Grab todays entries
    }

        // Return value summed up by day
    @SuppressLint("SimpleDateFormat")
    fun getToday():LiveData<Int> {
        val today = SimpleDateFormat("yyyyMMdd").format(Date().time)
        return dataRepository.getDay(today)
    }

    fun getDays(filtered: Boolean): List<Data> {
        if (filtered) getFilter()
        lateinit var items:List<Data>
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            items = if (filtered) dataRepository.getDays(filterStart, filterEnd)
            else dataRepository.getDays( 0, 0)
        }
            j.join() }
        return items
    }

    override fun createPdfDocument(filtered: Boolean): PdfDocument? {
        if (getSize(filtered) == 0) {
            userOutputService.showAndHideMessageForLong( app.getString(R.string.water) + " " + app.getString(R.string.noDataToExport))
            return null
        }
        val userName = preferences.getString(SettingsActivity.KEY_PREF_USER, "")
        val waterUnit = preferences.getString(SettingsActivity.KEY_PREF_WATER_UNIT, WATER_UNIT_DEFAULT)
        val highlightValues = preferences.getBoolean(SettingsActivity.KEY_PREF_COLOUR, false)
        val waterThreshold = preferences.getString(SettingsActivity.KEY_PREF_WATER_THRESHOLD, WATER_THRESHOLD_DEFAULT)!!.toInt()

        val document = PdfDocument()

        var pageNumber = 1
        var i: Int
        var pageInfo: PdfDocument.PageInfo?
        var page: PdfDocument.Page
        var canvas: Canvas
        val formatedDate = DateFormat.getDateInstance(DateFormat.SHORT).format(Calendar.getInstance().time) + " - " + DateFormat.getTimeInstance(DateFormat.SHORT).format(Calendar.getInstance().time)

        val pdfPaint = Paint()
        pdfPaint.isFakeBoldText = false
        pdfPaint.color = Color.BLACK

        val pdfPaintHighlight = Paint()
        pdfPaintHighlight.isFakeBoldText = true

        val paintRed = Paint()
        paintRed.color = Color.RED

        // Black & White instead of colour
        val blackAndWhite = true

        // -----------

        // crate a A4 page description
        pageInfo = PdfDocument.PageInfo.Builder(595, 842, pageNumber).create()
        page = document.startPage(pageInfo)
        canvas = page.canvas
        pdfRightBorder = canvas.width - pdfLeftBorder
        pdfDataBottom = canvas.height - 15

        val pdfHeaderDateColumn = pdfRightBorder - 150

        var headerText = app.getString(R.string.waterReportTitle)
        if (userName != null) headerText = headerText + " " + app.getString(R.string.forString) + " " + userName

        //                Log.d("--------------- Debug", "HeaderText:" + headerText);
        canvas.drawText(headerText, pdfLeftBorder.toFloat(), pdfHeaderTop.toFloat(), pdfPaint)
        canvas.drawText(app.getString(R.string.date) + ": " + formatedDate, pdfHeaderDateColumn.toFloat(), pdfHeaderTop.toFloat(), pdfPaint)
        canvas.drawLine(pdfLeftBorder.toFloat(), pdfHeaderBottom.toFloat(), pdfRightBorder.toFloat(), pdfHeaderBottom.toFloat(), pdfPaint)
        canvas.drawLine(pdfLeftBorder.toFloat(), pdfDataBottom.toFloat(), pdfRightBorder.toFloat(), pdfDataBottom.toFloat(), pdfPaint)

        i = pdfDataTop
        // Print header in bold
        canvas.drawText(app.getString(R.string.date), pdfLeftBorder.toFloat(), i.toFloat(), pdfPaintHighlight)
        canvas.drawText(app.getString(R.string.water) + " (" + waterUnit + ")", pdfDataTab.toFloat(), i.toFloat(), pdfPaintHighlight)

        // Todo: Add comment field to pdf
        // ------------
        var left = pdfLeftBorder // Value changes based on column
        var activeColumn = 1
        val column2 = canvas.width / 2 + left

        val summarizedPDF = preferences.getBoolean(SettingsActivity.KEY_PREF_SUMMARYPDF, WATER_SHOW_PDF_SUMMARY_DEFAULT)
        val items: List<Data> = if (summarizedPDF) {
            getDays(filtered)
        }
        else {
            getItems("ASC", filtered)
        }

        for (wi in items) {
            i += pdfLineSpacing
            if (i > pdfDataBottom) {
                // Second column
                if (activeColumn == 1) {
                    activeColumn = 2
                    i = pdfDataTop
                    // Column separator
                    pdfPaint.color = Color.DKGRAY
                    canvas.drawLine(column2.toFloat(), pdfHeaderBottom.toFloat(), column2.toFloat(), pdfDataBottom.toFloat(), pdfPaint)
                    pdfPaint.color = Color.BLACK
                    left = column2 + pdfLeftBorder
                    canvas.drawText(app.getString(R.string.date), left.toFloat(), i.toFloat(), pdfPaintHighlight)
                    canvas.drawText(app.getString(R.string.water) + " (" + waterUnit + ")", (left + pdfDataTab).toFloat(), i.toFloat(), pdfPaintHighlight)
                } else {
                    document.finishPage(page)
                    pageNumber += 1
                    activeColumn = 1

                    // crate a A4 page description
                    pageInfo = PdfDocument.PageInfo.Builder(595, 842, pageNumber).create()
                    page = document.startPage(pageInfo)
                    canvas = page.canvas
                    left = pdfLeftBorder

                    canvas.drawText(headerText, left.toFloat(), pdfHeaderTop.toFloat(), pdfPaint)
                    canvas.drawText(app.getString(R.string.date) + ": " + formatedDate, pdfHeaderDateColumn.toFloat(), pdfHeaderTop.toFloat(), pdfPaint)
                    canvas.drawLine(left.toFloat(), pdfHeaderBottom.toFloat(), pdfRightBorder.toFloat(), pdfHeaderBottom.toFloat(), pdfPaint)
                    canvas.drawLine(left.toFloat(), pdfDataBottom.toFloat(), pdfRightBorder.toFloat(), pdfDataBottom.toFloat(), pdfPaint)
                    i = pdfDataTop
                    canvas.drawText(app.getString(R.string.date), left.toFloat(), i.toFloat(), pdfPaint)
                    canvas.drawText(app.getString(R.string.water) + " (" + waterUnit + ")", (left + pdfDataTab).toFloat(), i.toFloat(), pdfPaintHighlight)
                    // ------------
                }
                i += pdfLineSpacing
            }
            canvas.drawText(toStringDate(wi.timestamp), left.toFloat(), i.toFloat(), pdfPaint)
            if (!summarizedPDF) canvas.drawText(toStringTime(wi.timestamp), left + pdfTimeTab.toFloat(), i.toFloat(), pdfPaint)

            val value = try {
                wi.value1.toInt()
            } catch  (e: NumberFormatException) {
                0
            }
            if (highlightValues and (value >= waterThreshold)) {
                if (blackAndWhite) {
                    canvas.drawText(wi.value1, left + pdfDataTab.toFloat(), i.toFloat(), pdfPaintHighlight)
                } else { // Colour
                    canvas.drawText(wi.value1, left +pdfDataTab.toFloat(), i.toFloat(), paintRed)
                }
            }
            else {
                canvas.drawText(wi.value1, left + pdfDataTab.toFloat(), i.toFloat(), pdfPaint)
            }
        }
        // finish the page
        document.finishPage(page)
        return document
    }
}