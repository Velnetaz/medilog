package com.zell_mbc.medilog.water

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.zell_mbc.medilog.utillity.Preferences
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.MainActivity.Companion.WATER
import com.zell_mbc.medilog.MainActivity.Companion.WATER_UNIT_DEFAULT
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.databinding.WaterinfoformBinding
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.weight.WeightViewModel
import java.text.DateFormat

class WaterInfoFragment : Fragment() {
    private var _binding: WaterinfoformBinding? = null
    private val binding get() = _binding!!
    private lateinit var userOutputService : UserOutputService

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = WaterinfoformBinding.inflate(inflater, container, false)
        initializeService(binding.root)

        return binding.root
    }
    private fun initializeService(view: View) {
        userOutputService = UserOutputServiceImpl(requireContext(),view)
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private lateinit var viewModel: WaterViewModel  //by viewModels() //factoryProducer = { SavedStateViewModelFactory(this, ) })

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(requireActivity()).get(WaterViewModel::class.java)
        viewModel.init(WATER)

        val preferences = Preferences.getSharedPreferences(requireContext())
        val waterUnit = " " + preferences.getString(SettingsActivity.KEY_PREF_WATER_UNIT, WATER_UNIT_DEFAULT)

        val count = viewModel.getSize(true)
        if (count == 0) {
            userOutputService.showMessageAndWaitForLong(getString(R.string.noDataToShow))
            return
        }

        // If Weight is logged calculate actual value
        //       binding.tvRecommendedWaterValue.text = getString(R.string.needWeight)
        val viewModel2 = ViewModelProvider(requireActivity()).get(WeightViewModel::class.java)
        viewModel2.init(WATER)
        val lastWeight = viewModel2.getLast(true)
        binding.tvRecommendedWaterValue.text = ""
        if (lastWeight != null) {
            val weight = try {
                lastWeight.value1.toFloat()
            } catch (e: NumberFormatException) {
                0f
            }
            if (weight > 0f) {
                val recommendedDialyIntake = "%.2f".format(weight * 30 / 1000) // In Liter
                // Todo: Handle imperial values
                // If water unit = oz 35.19503 different oz
                val s = getString(R.string.recommendedDailyWaterIntake) + " $recommendedDialyIntake l"
                binding.tvRecommendedWaterValue.text = s
                binding.tvFormula.text = getString(R.string.waterFormula)
            }
        }
        // Measurements
        var s = if ((viewModel.filterStart + viewModel.filterEnd) == 0L) getString(R.string.measurementsInDB) + " $count"
        else getString(R.string.measurementsInFilter) + " $count"
        binding.tvMeasurementCount.text = s

        val min = viewModel.getMinValue1(true)
        val max = viewModel.getMaxValue1(true)
        s = getString(R.string.minMaxValues) + " $min - $max $waterUnit"
        binding.tvMinMax.text = s

        val dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
        s = getString(R.string.timePeriod)
        val last = viewModel.getLast(true)
        if (last != null) {
            val endDate = dateFormat.format(last.timestamp)

            val first = viewModel.getFirst(true)
            val startDate = dateFormat.format(first?.timestamp)
            s += " $startDate - $endDate"
        }
        binding.tvTimePeriod.text = s
    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
        viewModel.deleteTmpItem() // Deletes all items with comment == tmpComment
    }
}
