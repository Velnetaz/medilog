package com.zell_mbc.medilog.custom

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.zell_mbc.medilog.MainActivity

class CustomChartActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainActivity.setTheme(this)

     supportFragmentManager.beginTransaction()
                .replace(android.R.id.content, CustomChartFragment())
                .commit()
    }
}