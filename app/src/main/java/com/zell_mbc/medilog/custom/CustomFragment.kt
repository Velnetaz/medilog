package com.zell_mbc.medilog.custom

import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import com.zell_mbc.medilog.*
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.data.SettingsViewModel
import com.zell_mbc.medilog.data.TabFragment
import com.zell_mbc.medilog.databinding.CustomTabBinding
import com.zell_mbc.medilog.diary.DiaryEditActivity
import com.zell_mbc.medilog.services.user.UserOutputService
import com.zell_mbc.medilog.services.user.UserOutputServiceImpl
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utillity.Preferences
import java.sql.Types.FLOAT
import java.sql.Types.INTEGER
import java.text.DateFormat
import java.util.*

class CustomFragment : TabFragment(), CustomListAdapter.ItemClickListener {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: CustomTabBinding? = null
    private val binding get() = _binding!!

    private var adapter: CustomListAdapter? = null

    private var valueDataType = 0
    private var tabLabel = ""

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = CustomTabBinding.inflate(inflater,container,false)
        initializeService(binding.root)
        return binding.root
    }


    private fun addItem() {

        if (!quickEntry) {
            val tmpItem = Data(0, Date().time, MainActivity.tmpComment, viewModel.dataType, "","","", "")
            val itemID = viewModel.insert(tmpItem)
            if (itemID  != 0L) editItem(itemID.toInt())
            else userOutputService.showMessageAndWaitForLong("No entry with tmp value found!")
            return
        }

        // Check empty variables
        val customValue = binding.etCustom.text.toString()
        val commentValue = binding.etComment.text.toString()
        if (customValue.isEmpty()) {
            //           userOutputService.showMessageAndWaitForLong(getString(R.string.customMissing))
            return
        }

        when (valueDataType) {
        INTEGER -> {
            try {
                val i = customValue.toInt()
                if (i <= 0) {
                    userOutputService.showMessageAndWaitForLong(this.getString(R.string.invalid) + " " + tabLabel + " " + this.getString(R.string.value) + " $customValue")
                    return
                }
            }
            catch (e: Exception) {
                userOutputService.showMessageAndWaitForLong("Exception: " + this.getString(R.string.invalid) + " " + tabLabel + " " + this.getString(R.string.value) + " $customValue")
                return
            }
        }
            FLOAT -> {
                try {
                    val f = customValue.toFloat()
                    if (f <= 0f) {
                        userOutputService.showMessageAndWaitForLong(this.getString(R.string.invalid) + " " + tabLabel + " " + this.getString(R.string.value) + " $customValue")
                        return
                    }
                }
                catch (e: Exception) {
                    userOutputService.showMessageAndWaitForLong("Exception: " + this.getString(R.string.invalid) + " " + tabLabel + " " + this.getString(R.string.value) + " $customValue")
                    return
                }
             }
        }

        val item = Data(0,  Date().time,commentValue, viewModel.dataType, customValue, "","","") // Start with empty item

        viewModel.insert(item)

        if ((viewModel.filterEnd > 0L) && (viewModel.filterEnd < item.timestamp)) userOutputService.showMessageAndWaitForDuration(getString(R.string.filteredOut), 4000)

        binding.etCustom.setText("")
        binding.etComment.setText("")

        // Close keyboard after entry is done
        (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken, 0)
    }

    override fun editItem(index: Int) {
        val intent = Intent(requireContext(), CustomEditActivity::class.java)
        launchActivity(intent, index)
    }

    override fun onItemClick(view: View?, position: Int) {
        val item = adapter?.getItemAt(position)
        item ?: return
        itemClicked(item)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val preferences = Preferences.getSharedPreferences(requireContext())

        quickEntry = preferences.getBoolean(SettingsActivity.KEY_PREF_QUICKENTRY, true)

        setColourStyle(binding.btAdd)
        binding.btAdd.setColorFilter(Color.WHITE)

        val settings = ViewModelProvider(this).get(SettingsViewModel::class.java)
        settings.init()

        viewModel = ViewModelProvider(requireActivity()).get(CustomViewModel::class.java)  // This should return the MainActivity ViewModel
        viewModel.items.observe(requireActivity(), { data -> data?.let { adapter!!.setItems(it) } })
        viewModel.dataType = 6

        var unit = settings.getString(viewModel.dataType, "UNIT")
        if (unit == null) unit = ""

        if (!quickEntry) { // Hide quick entry fields
            binding.etCustom.visibility = View.GONE
            binding.etComment.visibility = View.GONE
            binding.textUnit.visibility = View.GONE
        }
        else {
            binding.etCustom.visibility = View.VISIBLE
            binding.etComment.visibility = View.VISIBLE
            binding.textUnit.visibility = View.VISIBLE
            binding.textUnit.text = unit
        }

        val layoutManager = LinearLayoutManager(requireContext())
        binding.rvCustomList.layoutManager = layoutManager
        valueDataType = settings.getInt(viewModel.dataType, "VALUE_DATATYPE")
        tabLabel = settings.getString(viewModel.dataType, "TAB_LABEL")

        when (valueDataType) {
            INTEGER -> binding.etCustom.hint = "000"
            FLOAT   -> binding.etCustom.hint = "000.0"
        }

        val lowerThreshold  = settings.getInt(viewModel.dataType, "LOWER_THRESHOLD")
        val upperThreshold  = settings.getInt(viewModel.dataType, "UPPER_THRESHOLD")
        var highlightValues = settings.getBoolean(viewModel.dataType, "HIGHLIGHT_VALUES")
        if (highlightValues == null) highlightValues = false
        adapter = CustomListAdapter(requireContext(),
                unit,
                highlightValues,
                lowerThreshold,
                upperThreshold,
                valueDataType)

        adapter!!.setClickListener(this)
        binding.rvCustomList.adapter = adapter
        val dividerItemDecoration = DividerItemDecoration(binding.rvCustomList.context, layoutManager.orientation)
        binding.rvCustomList.addItemDecoration(dividerItemDecoration)

        // Respond to click events
        binding.btAdd.setOnClickListener { addItem() }
        setColourStyle(binding.btInfo, true)
        setColourStyle(binding.btChart, true)

        binding.btChart.setOnClickListener(View.OnClickListener {
            if (context == null) return@OnClickListener
            if (viewModel.getSize(true) < 2) {
                userOutputService.showMessageAndWaitForLong(requireContext().getString(R.string.notEnoughDataForChart))
                return@OnClickListener
            }

            val intent = Intent(requireContext(), CustomChartActivity::class.java)
            startActivity(intent)
        })

        binding.etCustom.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if ((event.action == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                addItem()
                return@OnKeyListener true
            }
            false
        })

    }
}