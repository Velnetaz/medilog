package com.zell_mbc.medilog.services.user

import android.content.Context
import android.graphics.Color
import android.view.View
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG
import android.widget.Toast.LENGTH_SHORT
import com.google.android.material.snackbar.Snackbar

class UserOutputServiceImpl(val context: Context, val view: View?) : UserOutputService {

    companion object {
        const val ACTION_TEXT = "Action"
    }


    override fun showAndHideMessageForLong(messageAsResourceId: Int) {
        showToastWithDuration(messageAsResourceId, LENGTH_LONG)
    }

    override fun showAndHideMessageForLong(messageAsCharSequence: CharSequence) {
        showToastWithDuration(messageAsCharSequence, LENGTH_LONG)
    }

    override fun showAndHideMessageForShort(messageAsResourceId: Int) {
        showToastWithDuration(messageAsResourceId, LENGTH_SHORT)
    }

    override fun showAndHideMessageForShort(messageAsCharSequence: CharSequence) {
        showToastWithDuration(messageAsCharSequence, LENGTH_SHORT)
    }

    override fun showMessageAndWaitForLong(messageAsString: CharSequence) {
        showASnackBarWithNoActionListenerWithDuration(messageAsString,Snackbar.LENGTH_LONG)
    }

    override fun showMessageAndWaitForDuration(messageAsCharSequence: CharSequence, durationAsMilliseconds: Int) {
        showASnackBarWithNoActionListenerWithDuration(messageAsCharSequence,durationAsMilliseconds)
    }

    override fun showMessageAndWaitForDurationAndAction(messageAsCharSequence: CharSequence, durationAsMilliseconds: Int, actionMessageAsCharSequence: CharSequence, actionListener: View.OnClickListener, actionTextColor: Int) {
        if (view == null)
            return

        Snackbar
                .make(view, messageAsCharSequence, durationAsMilliseconds)
                .setAction(actionMessageAsCharSequence, actionListener)
                .setActionTextColor(Color.RED)
                .show()
    }

    override fun showMessageAndWaitForLong(messageAsResourceId: Int) {
        showASnackBarWithNoActionListenerWithDuration(messageAsResourceId)
    }

    private fun showToastWithDuration(messageAsResourceId: Int, duration: Int) =
            Toast.makeText(context, context.getString(messageAsResourceId), duration).show()

    private fun showToastWithDuration(messageAsCharSequence: CharSequence, duration: Int) =
            Toast.makeText(context, messageAsCharSequence, duration).show()

    private fun showASnackBarWithNoActionListenerWithDuration(messageAsCharSequence: CharSequence, duration: Int) {
        if (view == null)
            return
        Snackbar.make(view, messageAsCharSequence, duration).setAction(ACTION_TEXT, null).show()
    }

    private fun showASnackBarWithNoActionListenerWithDuration(messageAsResourceId: Int) {
        if (view == null)
            return
        Snackbar.make(view, messageAsResourceId, Snackbar.LENGTH_LONG).setAction(ACTION_TEXT, null).show()
    }
}