package com.zell_mbc.medilog.weight

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnFocusChangeListener
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.zell_mbc.medilog.*
import com.zell_mbc.medilog.MainActivity.Companion.WEIGHT_UNIT_DEFAULT
import com.zell_mbc.medilog.data.Data
import com.zell_mbc.medilog.data.TabFragment
import com.zell_mbc.medilog.databinding.WeightTabBinding
import com.zell_mbc.medilog.settings.SettingsActivity
import java.util.*

class WeightFragment : TabFragment(), WeightListAdapter.ItemClickListener {
    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    private var _binding: WeightTabBinding? = null
    private val binding get() = _binding!!
    private var adapter: WeightListAdapter? = null

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = WeightTabBinding.inflate(inflater, container, false)

        initializeService(binding.root)

        return binding.root
    }

    //View Binding (https://developer.android.com/topic/libraries/view-binding)
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onItemClick(view: View?, position: Int) {
        val item = adapter?.getItemAt(position)
        item ?: return
        itemClicked(item)
    }

    override fun editItem(index: Int) {
        val intent = Intent(requireContext(), WeightEditActivity::class.java)
        launchActivity(intent, index)
    }

    private fun addItem() {
        if (!quickEntry) {
            val tmpItem = Data(0, Date().time, MainActivity.tmpComment, viewModel.dataType, "", "", "", "")
            val itemID = viewModel.insert(tmpItem)
            if (itemID  != 0L) editItem(itemID.toInt())
            else userOutputService.showMessageAndWaitForLong("No entry with tmp value found!")
            return
        }

        // Check empty variables
        val weightValue = binding.etWeight.text.toString()
        val commentValue = binding.etComment.text.toString()
        if (weightValue.isEmpty()) {
            userOutputService.showMessageAndWaitForLong(getString(R.string.weightMissing))
            return
        }
        var w = 0f
        try {
            w = weightValue.toFloat()
            if (w <= 0f) {
                userOutputService.showMessageAndWaitForLong(this.getString(R.string.invalid) + " " + this.getString(R.string.weight) + " " + this.getString(R.string.value) + " $w")
                return
            }
        } catch (e: Exception) {
            userOutputService.showMessageAndWaitForLong("Exception: " + this.getString(R.string.invalid) + " " + this.getString(R.string.weight) + " " + this.getString(R.string.value) + " $w")
            return
        }
        val item = Data(0, Date().time, commentValue, viewModel.dataType, w.toString(), "", "", "") // Start with empty item

        viewModel.insert(item)

        if ((viewModel.filterEnd > 0L) && (viewModel.filterEnd < item.timestamp)) userOutputService.showMessageAndWaitForDuration(getString(R.string.filteredOut), 4000)

        binding.etWeight.setText("")
        binding.etComment.setText("")

        // Close keyboard after entry is done
        (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(requireView().windowToken, 0)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setColourStyle(binding.btAdd)

        binding.btAdd.setColorFilter(Color.WHITE)

        if (!quickEntry) { // Hide quick entry fields
            binding.etWeight.visibility = View.GONE
            binding.etComment.visibility = View.GONE
            binding.textUnit.visibility = View.GONE
        }
        else {
            binding.etWeight.visibility = View.VISIBLE
            binding.etComment.visibility = View.VISIBLE
            binding.textUnit.visibility = View.VISIBLE
            binding.textUnit.text = preferences.getString(SettingsActivity.KEY_PREF_WEIGHT_UNIT, WEIGHT_UNIT_DEFAULT)
        }

        val layoutManager = LinearLayoutManager(requireContext())
        binding.rvWeightList.layoutManager = layoutManager

        adapter = WeightListAdapter(requireContext())
        viewModel = ViewModelProvider(requireActivity()).get(WeightViewModel::class.java)  // This should return the MainActivity ViewModel
        viewModel.items.observe(requireActivity(), { data -> data?.let { adapter!!.setItems(it) } })

        adapter!!.setClickListener(this)
        binding.rvWeightList.adapter = adapter
        val dividerItemDecoration = DividerItemDecoration(binding.rvWeightList.context, layoutManager.orientation)
        binding.rvWeightList.addItemDecoration(dividerItemDecoration)

        // Respond to click events
        binding.btAdd.setOnClickListener { addItem() }

        setColourStyle(binding.btInfo, true)
        setColourStyle(binding.btChart, true)

        binding.btInfo.setOnClickListener(View.OnClickListener {
            context ?: return@OnClickListener
            val intent = Intent(requireContext(), WeightInfoActivity::class.java)
            startActivity(intent)
        })

        binding.btChart.setOnClickListener(View.OnClickListener {
            if (context == null) return@OnClickListener
            if (viewModel.getSize(true) < 2) {
                userOutputService.showMessageAndWaitForLong(requireContext().getString(R.string.notEnoughDataForChart))
                return@OnClickListener
            }

            val intent = Intent(requireContext(), WeightChartActivity::class.java)
            startActivity(intent)
        })

        binding.etWeight.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if ((event.action == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                addItem()
                return@OnKeyListener true
            }
            false
        })

//        binding.etWeight.setOnFocusChangeListener(OnFocusChangeListener { v, hasFocus ->
//            binding.etWeight.requestFocus()
//        })
    }
}