package com.zell_mbc.medilog.weight

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.DashPathEffect
import android.graphics.Paint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.androidplot.util.PixelUtils
import com.androidplot.xy.*
import com.zell_mbc.medilog.MainActivity.Companion.KEY_PREF_WEIGHT_BAR_CHART_DEFAULT
import com.zell_mbc.medilog.MainActivity.Companion.KEY_PREF_WEIGHT_DAY_STEPPING_DEFAULT
import com.zell_mbc.medilog.MainActivity.Companion.SHOW_WEIGHT_GRID_DEFAULT
import com.zell_mbc.medilog.MainActivity.Companion.SHOW_WEIGHT_LEGEND_DEFAULT
import com.zell_mbc.medilog.MainActivity.Companion.SHOW_WEIGHT_THRESHOLD_DEFAULT
import com.zell_mbc.medilog.MainActivity.Companion.WEIGHT
import com.zell_mbc.medilog.MainActivity.Companion.WEIGHT_LINEAR_TRENDLINE_DEFAULT
import com.zell_mbc.medilog.MainActivity.Companion.WEIGHT_MOVING_AVERAGE_SIZE_DEFAULT
import com.zell_mbc.medilog.MainActivity.Companion.WEIGHT_MOVING_AVERAGE_TRENDLINE_DEFAULT
import com.zell_mbc.medilog.MainActivity.Companion.WEIGHT_THRESHOLD_DEFAULT
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.settings.SettingsActivity
import com.zell_mbc.medilog.utillity.*
import java.text.*
import java.util.*
import kotlin.math.roundToInt


// Chart manual
// https://github.com/halfhp/androidplot/blob/master/docs/xyplot.md
class WeightChartFragment : Fragment() {
    private var weightThreshold = ArrayList<Float>()
    private var weights = ArrayList<Float>()
    private var linearTrend = ArrayList<Float>()
    private var movingAverage = ArrayList<Float>()
    private var period = 5 // Minimum value = 2


    private fun calculateMovingAverage() {
        val sample = Array(size = period, init = { 0f }) // Create array of float, with all values set to 0
        val n = weights.size
        var sma: Float

        // the first n values in the sma will be off -> set them to the first weight value
        for (i in 0 until period) {
            sample[i] = weights[0]
        }

        for (i in 0 until n) {
            for (ii in 0..period-2) {
                sample[ii] = sample[ii + 1]
            }
            sample[period - 1] = weights[i]

            sma = 0f
            for (ii: Int in 0 until period) {
                sma += sample[ii]
            }
            sma /= period
            movingAverage.add(sma)
        }
    }

    private fun calculateLinearTrendLine() {
        // https://classroom.synonym.com/calculate-trendline-2709.html
        var a = 0f
        val b: Float
        var b1 = 0
        var b2 = 0f
        var c = 0
        val d: Float
        val e: Float
        val f: Float
        val g: Float
        val m: Float
        val n = weights.size
        for (i in 1..n) {
            a += i * weights[i - 1]
            b1 += i
            b2 += weights[i - 1]
            c += i * i
        }
        a *= n
        b = b1 * b2
        c *= n
        d = b1 * b1.toFloat()
        m = (a - b) / (c - d)
        e = b2
        f = m * b1
        g = (e - f) / n
        var value: Float
        for (i in 1..n) {
            value = m * i + g
            linearTrend.add(value)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.weight_chart, container, false)
    }

    @SuppressLint("SimpleDateFormat")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // create a couple arrays of y-values to plot:
        val labels = ArrayList<String>()
        var wMax = 0f
        var wMin = 1000f

        val preferences = Preferences.getSharedPreferences(requireContext())

        val daySteppingMode = preferences.getBoolean(SettingsActivity.KEY_PREF_WEIGHT_DAY_STEPPING, KEY_PREF_WEIGHT_DAY_STEPPING_DEFAULT)
        val barChart = preferences.getBoolean(SettingsActivity.KEY_PREF_WEIGHT_BAR_CHART, KEY_PREF_WEIGHT_BAR_CHART_DEFAULT)
        var sTmp: String

        var fTmp: Float
        val simpleDate = SimpleDateFormat("MM-dd")
        val lastDate = Calendar.getInstance()
        var currentDate: Date?

        val viewModel = ViewModelProvider(this).get(WeightViewModel::class.java)
        viewModel.init(WEIGHT)

        val items = viewModel.getItems("ASC", filtered = true)
        for (wi in items) {
            sTmp = simpleDate.format(wi.timestamp)

            // Chart stepping by day
            if (daySteppingMode) {
                // Fill gap
                currentDate = try {
                    simpleDate.parse(sTmp)
                } catch (e: ParseException) {
                    continue
                }

                if (currentDate != null) {
                    while (labels.size > 0 && currentDate > lastDate.time) {
                        sTmp = simpleDate.format(lastDate.time)
                        labels.add(sTmp)
                        Log.d("--------------- Debug", "GapDate: $sTmp")
                        weights.add(0f)
                        lastDate.add(Calendar.DAY_OF_MONTH, 1)
                    }
                }
                if (currentDate != null) lastDate.time = currentDate
            }
            labels.add(sTmp)

            val w = try {
                wi.value1.toFloat()
            } catch (e: NumberFormatException) {
                0f
            }
            weights.add(w)
            // Keep min and max values
            fTmp = wi.value1.toFloat()
            if (fTmp > wMax) {
                wMax = fTmp
            }
            if (fTmp < wMin) {
                wMin = fTmp
            }
        }
        if (weights.size == 0) {
            return
        }

        // If threshold is set create dedicated chart, otherwise show as origin
        val threshold = preferences.getBoolean(SettingsActivity.KEY_PREF_SHOW_WEIGHT_THRESHOLD, SHOW_WEIGHT_THRESHOLD_DEFAULT)
        val thresholdValue = (preferences.getString(SettingsActivity.KEY_PREF_WEIGHT_THRESHOLD, WEIGHT_THRESHOLD_DEFAULT))?.toFloat()
        if (threshold && thresholdValue != null) {
            for (item in weights) {
                weightThreshold.add(thresholdValue)
            }
        }

        // initialize our XYPlot reference:
        // https://github.com/halfhp/androidplot/blob/master/demoapp/src/main/java/com/androidplot/demos/TouchZoomExampleActivity.java
        val plot: XYPlot = view.findViewById(R.id.weightPlot)
        PanZoom.attach(plot, PanZoom.Pan.HORIZONTAL, PanZoom.Zoom.STRETCH_HORIZONTAL, PanZoom.ZoomLimit.MIN_TICKS)

        // Some formating moved to weight_chart.xml
        // https://github.com/halfhp/androidplot/blob/master/androidplot-core/src/main/res/values/attrs.xml

        val series1: XYSeries = SimpleXYSeries(weights, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, getString(R.string.weight))
        val series2: XYSeries = SimpleXYSeries(weightThreshold, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, getString(R.string.threshold))

        val backgroundColor = getBackgroundColor(requireContext())
        plot.graph.backgroundPaint.color = backgroundColor

        val fontSizeSmall: Int = getFontSizeSmallInPx(requireContext())

        val axisPaint = Paint()
        axisPaint.style = Paint.Style.FILL_AND_STROKE
        axisPaint.color = getTextColorPrimary(requireContext())
        axisPaint.textSize = fontSizeSmall.toFloat()
        axisPaint.isAntiAlias = true

        val gridPaint = Paint()
        gridPaint.style = Paint.Style.STROKE
        gridPaint.color = getTextColorSecondary(requireContext())
        gridPaint.isAntiAlias = false
        gridPaint.pathEffect = DashPathEffect(floatArrayOf(3f, 2f), 0F)

        // Format Grid
        val showGrid = preferences.getBoolean(SettingsActivity.KEY_PREF_SHOW_WEIGHT_GRID, SHOW_WEIGHT_GRID_DEFAULT)
        if (!showGrid) {
            plot.graph.domainGridLinePaint = null
            plot.graph.rangeGridLinePaint = null
        }
        else {
            plot.graph.domainGridLinePaint = gridPaint
            plot.graph.rangeGridLinePaint = gridPaint
        }

        // Format Legend
        val isLegendVisible = preferences.getBoolean(SettingsActivity.KEY_PREF_SHOW_WEIGHT_LEGEND, SHOW_WEIGHT_LEGEND_DEFAULT)
        plot.legend.isVisible = isLegendVisible
        plot.legend.isDrawIconBackgroundEnabled = false

        // Format Y-Axis
        val wMinBoundary = wMin.roundToInt()
        val wMaxBoundary = wMax.roundToInt()
        plot.setRangeBoundaries(wMinBoundary, wMaxBoundary, BoundaryMode.FIXED)
        plot.setUserRangeOrigin(wMinBoundary / 10 * 10) // Set to a 10er value
        plot.outerLimits.set(0, weights.size - 1, wMinBoundary, wMaxBoundary) // For pan&zoom

        // Show 5 lines not matter the values
        val yStepping = (wMaxBoundary - wMinBoundary + 1.0) / 5.0
        plot.setRangeStep(StepMode.INCREMENT_BY_VAL, yStepping)

        // X-Axis ###################
        plot.setDomainStep(StepMode.SUBDIVIDE, 5.0)
        plot.graph.getLineLabelStyle(XYGraphWidget.Edge.LEFT).format = DecimalFormat("####") // + weightUnit));  // Set integer y-Axis label

        // Line charts don't work without additional effort with date gaps, hence we switch to bar charts
        // Bar chart
        if (barChart) {
            val series1Format = BarFormatter(Color.BLUE, Color.BLUE)
            plot.addSeries(series1, series1Format)
        } else {
            val border = ContextCompat.getColor(requireContext(), R.color.chart_weight_border)
            val fill = ContextCompat.getColor(requireContext(), R.color.chart_weight_fill)
            val series1Format = LineAndPointFormatter(border, null, fill, null)
            plot.addSeries(series1, series1Format)
        }

        if (threshold) {
            val formatThreshold = LineAndPointFormatter(Color.BLUE, null, null, null)
            formatThreshold.linePaint.pathEffect = DashPathEffect(floatArrayOf( // always use DP when specifying pixel sizes, to keep things consistent across devices:
                    PixelUtils.dpToPix(20f),
                    PixelUtils.dpToPix(15f)), 0f)

            formatThreshold.isLegendIconEnabled = false
            formatThreshold.linePaint.strokeWidth = 2f
            plot.addSeries(series2, formatThreshold)
        }

        // Trendlines
        val weightLinarTrendLine = preferences.getBoolean(SettingsActivity.KEY_PREF_WEIGHT_LINEAR_TRENDLINE, WEIGHT_LINEAR_TRENDLINE_DEFAULT)
        val weightMovingAverageTrendLine = preferences.getBoolean(SettingsActivity.KEY_PREF_WEIGHT_MOVING_AVERAGE_TRENDLINE, WEIGHT_MOVING_AVERAGE_TRENDLINE_DEFAULT)

        if (weightLinarTrendLine) {
            calculateLinearTrendLine()

            val trendFormat = LineAndPointFormatter(Color.RED, null, null, null)
            val trendLine: XYSeries = SimpleXYSeries(linearTrend, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "Linear Trend")

            trendFormat.isLegendIconEnabled = false
            plot.addSeries(trendLine, trendFormat)
        }

        if (weightMovingAverageTrendLine) {
            period = preferences.getString(SettingsActivity.KEY_PREF_WEIGHT_MOVING_AVERAGE_SIZE, WEIGHT_MOVING_AVERAGE_SIZE_DEFAULT)!!.toInt()
            calculateMovingAverage()

            val trendFormat = LineAndPointFormatter(Color.RED, null, null, null)
            trendFormat.isLegendIconEnabled = false
            val simpleMovingAverage: XYSeries = SimpleXYSeries(movingAverage, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "Moving Average")
            plot.addSeries(simpleMovingAverage, trendFormat)
        }

        plot.graph.getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).format = object : Format() {
            override fun format(obj: Any, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer {
                val i = (obj as Number).toFloat().roundToInt()
                return toAppendTo.append(labels[i])
            }

            override fun parseObject(source: String, pos: ParsePosition): Any {
                return 0
            }
        }
    }
}