package com.zell_mbc.medilog.weight

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.zell_mbc.medilog.MainActivity

class WeightChartActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainActivity.setTheme(this)

     supportFragmentManager.beginTransaction()
                .replace(android.R.id.content, WeightChartFragment())
                .commit()
    }
}