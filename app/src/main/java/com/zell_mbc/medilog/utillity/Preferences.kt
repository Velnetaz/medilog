package com.zell_mbc.medilog.utillity

import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import androidx.preference.PreferenceManager
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import java.io.IOException
import java.security.GeneralSecurityException

class Preferences(val context: Context) {
    private var isEncrypted = false
    lateinit var preferences: SharedPreferences

    fun getManager(): SharedPreferences {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                val masterKey = MasterKey.Builder(context, MasterKey.DEFAULT_MASTER_KEY_ALIAS)
                        .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
                        .build()

                isEncrypted = true

                preferences = EncryptedSharedPreferences.create(
                        context,
                        "medilog_prefs",
                        masterKey,
                        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM)

                // Check if old sharedPreferences need to be migrated
                //if (!preferences.getBoolean("encryptedPreferences", false))
                //    migrate()
                return preferences
            } catch (e: GeneralSecurityException) {
                isEncrypted = false
                e.printStackTrace()
            } catch (e: IOException) {
                isEncrypted = false
                e.printStackTrace()
            }
            return PreferenceManager.getDefaultSharedPreferences(context)
        } else {
            return PreferenceManager.getDefaultSharedPreferences(context)
        }
    }

    private fun getSharedPreferences(): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(context)
    }

    companion object SingletonFactory {

        private var instance: Preferences? = null

        private fun getInstance(context: Context): Preferences? {
            if (instance == null) instance =
                    Preferences(context)
            return instance
        }

        fun getEncryptedOrSharedPreferences(context: Context): SharedPreferences{
            return getInstance(context)!!.getManager()
        }

        fun getSharedPreferences(context: Context): SharedPreferences{
            return getInstance(context)!!.getSharedPreferences()
        }
    }
}
