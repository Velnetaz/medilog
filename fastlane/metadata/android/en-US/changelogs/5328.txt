## v2.0.2, build 5328
New 

- Added signing certificate info to About screen
- Capture and show date of last backup in About dialog
- New translation: Norwegian Bokmål
- Split out backup preferences into separate screen
- Added backup overdue check
- Added autoBackup setting (disabled until 2.1.0)

Fixed

- Fixed weight data showing too many digits after migrating to v2.0
- Changed charts so all available space is used
- Fixed bug where the water chart/pdf would show individual values instead of a days summary
- Changed highlighting values in pdf from > to >=
- v2 weight chart: Zoom only timeline (x-axis), not the values (y-axis)

Known issues

- None
